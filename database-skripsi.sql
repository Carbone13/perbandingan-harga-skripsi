-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 25, 2018 at 04:39 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1-log
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kolokium`
--

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_admins`
--

CREATE TABLE `dsptesty_admins` (
  `id` int(11) NOT NULL,
  `fname` varchar(35) NOT NULL,
  `email` varchar(99) NOT NULL,
  `password` varchar(99) NOT NULL,
  `password_recover` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_admins`
--

INSERT INTO `dsptesty_admins` (`id`, `fname`, `email`, `password`, `password_recover`, `active`) VALUES
(1, 'Admin', 'admin@admin.com', '5fa93eeb355449241d61983b703b1ac3', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_categories`
--

CREATE TABLE `dsptesty_categories` (
  `id` int(3) NOT NULL,
  `name` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_categories`
--

INSERT INTO `dsptesty_categories` (`id`, `name`, `active`) VALUES
(1, 'Komputer & Aksesoris', 1),
(2, 'Handphone & Tablet', 1),
(3, 'PC Gaming', 1),
(4, 'Komputer & Aksesoris', 1),
(5, 'Komputer & Aksesoris', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_coupons`
--

CREATE TABLE `dsptesty_coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `off` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `off_type` int(11) NOT NULL,
  `order_min` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dsptesty_coupons`
--

INSERT INTO `dsptesty_coupons` (`id`, `code`, `off`, `off_type`, `order_min`, `active`) VALUES
(1, 'TEST', '2.00', 2, 2, 1),
(2, 'CODE18', '50.00', 2, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_custompages`
--

CREATE TABLE `dsptesty_custompages` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `indate` date NOT NULL,
  `level` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_custompages`
--

INSERT INTO `dsptesty_custompages` (`id`, `title`, `content`, `indate`, `level`, `active`) VALUES
(1, 'Contoh Halaman 1', '<p>Contoh Halaman 1</p>', '2018-03-26', 0, 1),
(2, 'Contoh Halaman 2', '<p>Contoh Halaman 2</p>', '2018-03-26', 1, 1),
(3, 'Contoh Halaman 3', '<h1>Contoh Halaman 3</h1>', '2018-03-26', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_news`
--

CREATE TABLE `dsptesty_news` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(300) NOT NULL,
  `date` datetime NOT NULL,
  `important` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `author` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_news`
--

INSERT INTO `dsptesty_news` (`id`, `title`, `content`, `date`, `important`, `active`, `author`) VALUES
(1, 'Selamat Datang di Perbandingan Harga', 'Kami akan meluncurkan pada tanggal 1 Januari 2019', '2018-03-10 00:00:00', 0, 1, 'Admin'),
(2, 'Peluncuran Pertama', 'Hai , Peluncuran Aplikasi Pertama Telah Di Terbitkan .', '2018-03-18 14:29:13', 0, 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_products`
--

CREATE TABLE `dsptesty_products` (
  `id` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `price_bl` int(11) NOT NULL DEFAULT '0',
  `price_blanja` int(11) NOT NULL DEFAULT '0',
  `price_lazada` int(11) NOT NULL DEFAULT '0',
  `short_des` varchar(200) DEFAULT NULL,
  `description` text NOT NULL,
  `gambar_produk` varchar(255) NOT NULL,
  `icon_img` varchar(99) DEFAULT NULL,
  `preview_img` varchar(255) DEFAULT NULL,
  `file` varchar(99) DEFAULT NULL,
  `sales` int(11) NOT NULL DEFAULT '0',
  `support` int(11) NOT NULL DEFAULT '0',
  `demo` varchar(255) DEFAULT NULL,
  `demo_bl` varchar(255) DEFAULT NULL,
  `demo_blanja` varchar(255) DEFAULT NULL,
  `demo_lazada` varchar(255) DEFAULT NULL,
  `featured` int(11) NOT NULL DEFAULT '0',
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '0',
  `dlname` varchar(255) DEFAULT NULL,
  `cat_id` int(3) NOT NULL DEFAULT '0',
  `subc_id` int(11) NOT NULL DEFAULT '0',
  `approved` int(11) NOT NULL DEFAULT '0',
  `stock_on` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '0',
  `views_off` int(11) NOT NULL DEFAULT '0',
  `reviews_off` int(11) NOT NULL DEFAULT '0',
  `downloadlim_on` int(11) DEFAULT NULL,
  `free` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_products`
--

INSERT INTO `dsptesty_products` (`id`, `name`, `price`, `price_bl`, `price_blanja`, `price_lazada`, `short_des`, `description`, `gambar_produk`, `icon_img`, `preview_img`, `file`, `sales`, `support`, `demo`, `demo_bl`, `demo_blanja`, `demo_lazada`, `featured`, `created`, `modified`, `active`, `views`, `dlname`, `cat_id`, `subc_id`, `approved`, `stock_on`, `stock`, `views_off`, `reviews_off`, `downloadlim_on`, `free`) VALUES
(16, 'Mikrotik RB750r2', 575000, 565000, 600000, 575000, '5x Ethernet Small Plastic Case 850MHz CPU 64MB RAM Most Affordable MPLS Router RouterOS L4 ...', '<p>5x Ethernet, Small plastic case, 850MHz CPU, 64MB RAM, Most affordable MPLS router, RouterOS L4</p>', 'https://ecs7.tokopedia.net/img/cache/700/product-1/2016/1/9/6967287/6967287_f710089e-e9b5-4ea3-9fae-790e35473a83.jpg', NULL, NULL, NULL, 0, 0, 'https://www.tokopedia.com/jayanetwork/mikrotik-rb750r2', 'https://www.bukalapak.com/p/komputer/aksesoris-226/network-tools/qnc91-jual-mikrotik-router-indoor-rb750r2', 'https://www.blanja.com/p/com/mikrotik-rb750r2-18478392', 'https://www.lazada.co.id/products/mikrotik-router-indoor-rb750r2-hex-lite-rb750-r2-i398270804-s437396790.html?search=1', 0, '2018-12-19', '2018-12-19', 1, 2, NULL, 1, 1, 0, 0, 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_reviews`
--

CREATE TABLE `dsptesty_reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` varchar(15) NOT NULL,
  `review` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_reviews`
--

INSERT INTO `dsptesty_reviews` (`id`, `product_id`, `user_id`, `review`, `rating`, `status`) VALUES
(1, 2, 'L3LVG98D', 'test', 5, 0),
(2, 2, 'L3LVG98D', 'I really like this product and support is amazing!', 5, 0),
(3, 2, 'L3LVG98D', 'Wow!!!!!!!', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_sales`
--

CREATE TABLE `dsptesty_sales` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `date` varchar(10) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `downloaded` int(11) NOT NULL DEFAULT '0',
  `downloadrem` int(11) DEFAULT NULL,
  `coupon` varchar(10) DEFAULT NULL,
  `method` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_settings`
--

CREATE TABLE `dsptesty_settings` (
  `setting` varchar(99) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_settings`
--

INSERT INTO `dsptesty_settings` (`setting`, `value`) VALUES
('currency', 'IDR'),
('currency_sym', 'Rp'),
('homepage_header', 'Perbandingan Harga'),
('homepage_subheader', 'Bandingkan harga produk favorit anda !'),
('paypal_email', 'test@test.com'),
('site_favicon', 'logomini.jpg'),
('site_logo', 'logo.png'),
('site_name', 'Perbandingan Harga'),
('support_email', 'test@test.com'),
('theme', 'primary'),
('txn', '1'),
('global_message', ''),
('alert_type', 'info'),
('show_card_sde', '0'),
('website_url', 'http://kolokium.muhamadghufron.com');

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_subcat`
--

CREATE TABLE `dsptesty_subcat` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET latin1 NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dsptesty_subcat`
--

INSERT INTO `dsptesty_subcat` (`id`, `cat_id`, `name`, `active`) VALUES
(1, 1, 'Networking', 1),
(2, 2, 'Handphone', 1),
(3, 3, 'Keyboard Gaming', 1),
(4, 1, 'Hard Disk SSD', 1),
(5, 1, 'Flash Disk', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_transactions`
--

CREATE TABLE `dsptesty_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `product` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL,
  `callback` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_users`
--

CREATE TABLE `dsptesty_users` (
  `id` int(11) NOT NULL,
  `fname` varchar(99) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(99) NOT NULL,
  `password` varchar(99) NOT NULL,
  `purchases` int(11) NOT NULL,
  `balance` varchar(11) DEFAULT NULL,
  `verified` int(11) NOT NULL,
  `allow_email` int(11) NOT NULL,
  `password_recover` int(1) NOT NULL,
  `profile` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `moderator` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsptesty_users`
--

INSERT INTO `dsptesty_users` (`id`, `fname`, `username`, `email`, `password`, `purchases`, `balance`, `verified`, `allow_email`, `password_recover`, `profile`, `created`, `moderator`, `active`) VALUES
(1, 'Test', 'test', 'test@test.com', '25d55ad283aa400af464c76d713c07ad', 0, '0', 0, 1, 1, '../system/assets/uploads/user-img/default.png', '2018-02-13 07:45:57', 0, 1),
(2, 'test1', 'staff', 'test@test123.com', '25d55ad283aa400af464c76d713c07ad', 0, '0', 0, 1, 1, '../system/assets/uploads/user-img/default.png', '2018-04-05 15:47:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dsptesty_wishlists`
--

CREATE TABLE `dsptesty_wishlists` (
  `w_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dsptesty_admins`
--
ALTER TABLE `dsptesty_admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_categories`
--
ALTER TABLE `dsptesty_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_coupons`
--
ALTER TABLE `dsptesty_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_custompages`
--
ALTER TABLE `dsptesty_custompages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_news`
--
ALTER TABLE `dsptesty_news`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `dsptesty_products`
--
ALTER TABLE `dsptesty_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_reviews`
--
ALTER TABLE `dsptesty_reviews`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `dsptesty_sales`
--
ALTER TABLE `dsptesty_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `used_id` (`user_id`);

--
-- Indexes for table `dsptesty_settings`
--
ALTER TABLE `dsptesty_settings`
  ADD UNIQUE KEY `setting_2` (`setting`),
  ADD KEY `setting` (`setting`);

--
-- Indexes for table `dsptesty_subcat`
--
ALTER TABLE `dsptesty_subcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_transactions`
--
ALTER TABLE `dsptesty_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsptesty_users`
--
ALTER TABLE `dsptesty_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`email`);

--
-- Indexes for table `dsptesty_wishlists`
--
ALTER TABLE `dsptesty_wishlists`
  ADD PRIMARY KEY (`w_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dsptesty_admins`
--
ALTER TABLE `dsptesty_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dsptesty_categories`
--
ALTER TABLE `dsptesty_categories`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dsptesty_coupons`
--
ALTER TABLE `dsptesty_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dsptesty_custompages`
--
ALTER TABLE `dsptesty_custompages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dsptesty_news`
--
ALTER TABLE `dsptesty_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dsptesty_products`
--
ALTER TABLE `dsptesty_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `dsptesty_reviews`
--
ALTER TABLE `dsptesty_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dsptesty_sales`
--
ALTER TABLE `dsptesty_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dsptesty_subcat`
--
ALTER TABLE `dsptesty_subcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dsptesty_transactions`
--
ALTER TABLE `dsptesty_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dsptesty_users`
--
ALTER TABLE `dsptesty_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dsptesty_wishlists`
--
ALTER TABLE `dsptesty_wishlists`
  MODIFY `w_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
