<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('system/config-global.php');

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $details = $product->details($id);
    $allreviews = $product->getReviews($id);
    if($details['cat_id']!='0'){
    $cat = $product->catdetails($details['cat_id']);
    $cat1 = $cat['name'];
    $cat2 = $cat['id'];
    }
    else{
        $cat1 = $l['no_category'];
    }
    if ($details) {
        $query = $DB_con->prepare("UPDATE dsptesty_products SET views = views + 1 WHERE id = ?");
        $query->execute(array($id));
    } else {
        display_post_not_found($id);
        exit();
    }

//userdelete
if(!empty($_GET['del']) && $user->is_loggedin()){
    
    $idcomm = $_GET['del'];
    $query = $DB_con->prepare("UPDATE dsptesty_reviews SET status = 0 WHERE id = ? AND user_id = ?");
    $query->execute(array($idcomm,$_SESSION['uid']));
    header('Location: item.php?id='.$id.'');
    exit();
    
}
//moderatordelete
if(!empty($_GET['delp']) && $userDetails['moderator'] == 1){
    
    $idcomm = $_GET['delp'];
    $query = $DB_con->prepare("UPDATE dsptesty_reviews SET status = 0 WHERE id = ?");
    $query->execute(array($idcomm));
    header('Location: item.php?id='.$id.'');
    exit();
    
}

$pageTitle = $details['name'];;

require_once('system/assets/header.php');

if(isset($_POST['addtowishlist'])){
    $addtw = $wishlist->add($_SESSION['uid'], $id);
}
?>
  <main role="main">

    	<?php if($user->is_loggedin()){
	if($purchases->is_purchased($_SESSION['uid'],$id)){
	echo '<div class="alert alert-danger mb-0" role="alert">'.$l['youalreadypurch'].'</div>';
	}
    	}
    	
	?>
	
	<?php echo ($details['active']=='2'?'<div class="alert alert-danger mb-0" role="alert">'.$l['item_paused'].'</div>':'');?> 
	
<div class="jumbotron p-0 pt-5 bg-light border-bottom">
  <div class="container">
      
    <div class="col-md-12 px-0">
        
        <div class="row p-15"> 
        <div class="p-15 pb-5"> 
        <img itemprop="image" class="pull-left thumb-lg m-r-md rounded mr-3" src="<?php echo $details['gambar_produk'];?>" alt="<?php echo $details['name']; ?>" width="80" height="80"> 
        </div> 
        <div class="col-sm-10"> 
        <h1 class="h2 text-dark" itemprop="name"><?php echo $details['name']; ?></h1> 
        <span itemprop="description" class=""><?php echo $details['short_des']; ?></span> 
        </div> 
        </div>
        

      <ul class="nav nav-tabs no-border fs-16 p-15">
        <li class="nav-item"> <a class="nav-link active" href="#"><?php echo $l['overview']; ?></a> </li>
         <?php if($details['reviews_off'] == '0'){?><li class="nav-item"> <a class="nav-link" data-toggle="modal" data-target="#exampleReview" href="#"><?php echo $l['reviews']; ?></a> </li><?php } else{ } ?>    
        <li class="nav-item"> <a data-toggle="modal" data-target="#exampleModal" class="nav-link" href="#"><?php echo $l['support']; ?></a> </li>
      </ul>
    </div>
  </div>
  </div>
</div>

<div class="container">
    
<div class="row p-15">
<div class="col-sm-8">
<div class="card text-center box-shadow">
  <div class="card-body p-0">
<img class="card-img-top" src="<?php echo $details['gambar_produk'];?>" alt="<?php echo $details['name']; ?>">
  </div>
  <div class="card-footer text-muted">
      <!--<div class="btn-group" role="group" aria-label="Basic example">
    <?php if(!empty($details['demo'])){ ?><a target="_blank" href="<?php echo $details['demo'];?>" class="btn btn-primary mr-2 rounded"><?php echo $l['live_preview']; ?></a><?php }?>
	<?php
	if($user->is_loggedin()){
	if($wishlist->is_alreadyadd($_SESSION['uid'],$id)){

	} else{ ?>  <form action="item.php?id=<?php echo $id;?>" method="POST">	<a>	<button name="addtowishlist" type="submit" class="btn btn-outline-primary"><?php echo $l['add_to_wish']; ?></button></a>
		</form> <?php }} ?>
		</div>-->
  </div>
</div>

<div class="card mt-3 box-shadow">
  <div class="card-header">
    <?php echo $l['overview']; ?>
  </div>
  <div class="card-body">
<?php echo $details['description']; ?>
  </div>
</div>


</div>
<div class="col-sm-4">

<?php if($details['free'] == '1'){?>
    <div class="card mb-3 box-shadow text-center">
      <div class="card-body">
        <h5 class="card-title"><?php echo $l['ffotw']; ?></h5>
        <p class="card-text"><?php echo $l['ffinfo']; ?></p>
        <a data-toggle="modal" class="btn btn-success btn-lg btn-block font-bold text-white mt-4" data-toggle="modal" data-target="#freeModal"><i class="fas fa-download"></i> <?php echo $l['download_now']; ?></a>
      </div>
    </div>
    <?php } ?>

<div class="card box-shadow">
  <div class="card-body">
    <div class="clearfix mb-4">
    <div class="text-center">
					<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="m-l-xs price">
					  <link itemprop="availability" href="http://schema.org/InStock">
					<span class="h2 text-right-xs " itemprop="priceCurrency" content="<?php echo $setting['currency'];?>"><?php echo $setting['currency_sym'];?></span><span class="h1 text-right-xs font-bold " data-licence="regular" itemprop="price" content="<?php echo $details['price'];?>"><?php echo $details['price'];?></span>						
					</span>
				  </div>
				  </div>
   <!-- <div class="clear m-t-md m-b-md text-md">   
				     <?php echo ($details['support']=='1'?'<i class="fa fa-check-circle text-success fa-fw"></i> '.$l['we_do_support'].'<br>':'<i class="fa fa-times-circle text-danger fa-fw"></i> '.$l['do_not_support'].'<br>');?> 
		            <i class="fa fa-check-circle text-success fa-fw"></i> <?php echo $l['future_updates']; ?><br>
		            <i class="fa fa-check-circle text-success fa-fw"></i> <?php echo $l['unlimited_downloads']; ?><br>
                </div>-->
   
    <i class="fa fa-check-cart text-success fa-fw"></i> Marketplace : Tokopedia<br>
    <button class="btn btn-primary btn-lg btn-block font-bold mt-4" type="submit">Beli Sekarang</button>
    
   <?php 
/* if($details['stock_on']=='1' && $details['stock'] <= 0) { 
      echo '<div class="alert alert-danger mt-3 mb-0" role="alert">'.$l['out_of_stock'].'</div>'; 
    } 
        elseif($details['stock_on']=='1' && $user->is_loggedin() && $purchases->is_purchased($_SESSION['uid'],$id)){
         echo '<div class="alert alert-danger mt-3 mb-0" role="alert">'.$l['already_purchased'].'</div>';
    }
    elseif($details['stock_on']=='1' && $details['stock'] >= 0) { 
      echo '<div class="alert alert-success mt-3 mb-0" role="alert">'.$l['in_stock'].'</div><a class="dsp-btn" data-pid="'.$id.'">'.$l['buy_now'].'</a>';
    } 
  
  
  
  if($details['stock_on']=='0' && $user->is_loggedin() && $purchases->is_purchased($_SESSION['uid'],$id)){
      echo '<div class="alert alert-danger mt-3 mb-0" role="alert">'.$l['already_purchased'].'</div>';
    }
  elseif($details['stock_on']=='0'){
     echo '<a class="dsp-btn" data-pid="'.$id.'">'.$l['buy_now'].'</a>'; 
  }*/
  
  
  /*if($details['stock_on']=='1' && $details['stock'] <= 0) { 
      echo '<div class="alert alert-danger mt-3 mb-0" role="alert">Out of Stock!</div>'; 
    } elseif($details['stock_on']=='1') { 
      echo '<div class="alert alert-success mt-3 mb-0" role="alert">In Stock!</div><a class="dsp-btn" data-pid="'.$id.'">Buy Now</a>';
    } else{
      echo '<a class="dsp-btn" data-pid="'.$id.'">Buy Now</a>';
    }*/?>
  </div>
  <!--<div class="card-footer text-muted text-center">
  </div>-->
</div>
  
  <div class="card box-shadow">
  <div class="card-body">
    <div class="clearfix mb-4">
    <div class="text-center">
					<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="m-l-xs price">
					  <link itemprop="availability" href="http://schema.org/InStock">
					<span class="h2 text-right-xs " itemprop="priceCurrency" content="<?php echo $setting['currency'];?>"><?php echo $setting['currency_sym'];?></span><span class="h1 text-right-xs font-bold " data-licence="regular" itemprop="price" content="<?php echo $details['price_bl'];?>"><?php echo $details['price_bl'];?></span>						
					</span>
				  </div>
				  </div>
   <!-- <div class="clear m-t-md m-b-md text-md">   
				     <?php echo ($details['support']=='1'?'<i class="fa fa-check-circle text-success fa-fw"></i> '.$l['we_do_support'].'<br>':'<i class="fa fa-times-circle text-danger fa-fw"></i> '.$l['do_not_support'].'<br>');?> 
		            <i class="fa fa-check-circle text-success fa-fw"></i> <?php echo $l['future_updates']; ?><br>
		            <i class="fa fa-check-circle text-success fa-fw"></i> <?php echo $l['unlimited_downloads']; ?><br>
                </div>-->
   
    <i class="fa fa-cart text-success fa-fw"></i> Marketplace : Bukalapak<br>
    <button class="btn btn-primary btn-lg btn-block font-bold mt-4" type="submit">Beli Sekarang</button>
    
   <?php 
/* if($details['stock_on']=='1' && $details['stock'] <= 0) { 
      echo '<div class="alert alert-danger mt-3 mb-0" role="alert">'.$l['out_of_stock'].'</div>'; 
    } 
        elseif($details['stock_on']=='1' && $user->is_loggedin() && $purchases->is_purchased($_SESSION['uid'],$id)){
         echo '<div class="alert alert-danger mt-3 mb-0" role="alert">'.$l['already_purchased'].'</div>';
    }
    elseif($details['stock_on']=='1' && $details['stock'] >= 0) { 
      echo '<div class="alert alert-success mt-3 mb-0" role="alert">'.$l['in_stock'].'</div><a class="dsp-btn" data-pid="'.$id.'">'.$l['buy_now'].'</a>';
    } 
  
  
  
  if($details['stock_on']=='0' && $user->is_loggedin() && $purchases->is_purchased($_SESSION['uid'],$id)){
      echo '<div class="alert alert-danger mt-3 mb-0" role="alert">'.$l['already_purchased'].'</div>';
    }
  elseif($details['stock_on']=='0'){
     echo '<a class="dsp-btn" data-pid="'.$id.'">'.$l['buy_now'].'</a>'; 
  }*/
  
  
  /*if($details['stock_on']=='1' && $details['stock'] <= 0) { 
      echo '<div class="alert alert-danger mt-3 mb-0" role="alert">Out of Stock!</div>'; 
    } elseif($details['stock_on']=='1') { 
      echo '<div class="alert alert-success mt-3 mb-0" role="alert">In Stock!</div><a class="dsp-btn" data-pid="'.$id.'">Buy Now</a>';
    } else{
      echo '<a class="dsp-btn" data-pid="'.$id.'">Buy Now</a>';
    }*/?>
  </div>
  <!--<div class="card-footer text-muted text-center">
  </div>-->
</div>

	<?php echo ($details['featured']=='2'?'<div class="alert alert-primary mt-3 mb-0" role="alert">'.$l['hasbeenfeat'].'</div>':'');?> 

<div class="card mt-3 box-shadow">
 <div class="card-header text-muted text-left">
 <?php echo $l['information']; ?>
  </div>
  <div class="table-responsive">
<table class="table table-striped mb-0">
  <tbody>

    <tr>
      <td class="col-xs-5">
         <?php echo $l['category']; ?>
      </td>
      <td class="col-xs-7"> <a href="<?php echo $setting['website_url']; ?>/category.php?id=<?php echo $cat2; ?>" title=""><?php echo $cat1; ?></a> </td>
    </tr>
    <tr>
      <td>
        <?php echo $l['first_release']; ?>
      </td>
      <td><?php 
      
       $created = $details['created'];
                                    $date = new DateTime($created);
                                    echo $date->format('j F Y');?>
                                    
                                    </td>
    </tr>
    <tr>
      <td>
        <?php echo $l['last_updated']; ?>
      </td>
      <td>
        <?php 
      
       $modified = $details['modified'];
                                    $date = new DateTime($modified );
                                    echo $date->format('j F Y');?>
      </td>
    </tr>
  </tbody>
</table>
  </div>
</div>

<div class="card mt-3 box-shadow">
<div class="card-body">
<i class="fas fa-shopping-cart"></i> <?php echo $details['sales'];?> Pengunjung Afiliasi
</div>
</div>

</div>

</div>

</div>
<!--Similar Products-->
<div class="container mt-5">

    <div class="wrapper mb-3 p-15">
	          <span class="font-weight-bold h4 m-t-sm"><?php echo $l['similarprod']?></span>
		</div>
 <div class="row p-15">
     <?php
     $similar = $product->getSimilarProducts($cat2);
      foreach( $similar as $row ) {
          $str = $row['name'];
          ?>
          
              <div class="col-lg-4 col-md-4 col-sm-6 mb-4">
    <div class="card box-shadow h-100">
        <div class="relativel">
            <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>">
                 <?php if($row['featured'] == 1){ ?>
            <div class="typebadge"><span class="bg-primary"><i class="fa fa-file mr-1" aria-hidden="true"></i><?php echo $l['featured']?></span></div>
            <?php } ?>
        <img class="card-img-top" src="<?php echo $row['gambar_produk']?>" alt="<?php echo $row['name']?>"></a>
        </div>
      <div class="card-body text-center">
          <h5 class="card-title mb-3"><?php echo $row['name']?></h5>
         <?php if($setting['show_card_sde'] == '1'){?> <p class="card-text"><?php echo $row['short_des']?></p><?php } ?>
    <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>" class="btn btn-sm btn-outline-secondary box-shadow"><?php echo $l['view_product']?></a>
      </div>
          <div class="card-footer">
<div class="clearfix">
 <?php if($row['views_off'] == '0'){?><a class="btn btn-sm float-left pr-0"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $row['views']; ?></a><?php } else{ } ?>
 <a class="btn btn-sm float-left"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo $row['sales'];?></a>
  <?php if($row['price'] == 0){ ?>
      <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } elseif($row['free'] == 1){ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><s><?php echo $setting['currency_sym'].$row['price'];?></s></button><button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } else{ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $setting['currency_sym'].$row['price'];?></button>
  <?php } ?>
</div>
    </div>
    </div>
  </div>
          
      <?php } ?>
    </div>  </div>

<?php if($details['free'] == '1'){?>
<!-- FreeModal -->
<div class="modal fade" id="freeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $l['freedl']; ?> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 <?php if($user->is_loggedin()){ ?>     <form id="free">
            <div class="form-group">
    <input type="hidden" class="form-control" name="name" placeholder="Enter Name" value="<?php echo $userDetails['fname']; ?>" ="">
  </div>
        <input type="hidden" class="form-control" name="uid" value="<?php echo $_SESSION['uid']; ?>">
      <input type="hidden" class="form-control" name="pid" value="<?php echo $details['id']; ?>">
  <div class="form-group">
    <input type="hidden" name="email" class="form-control" value="<?php echo $userDetails['email']; ?>" ="">
  </div>
  <button type="submit" class="btn btn-success btn-lg btn-block submit"><i class="fas fa-download"></i> <?php echo $l['download_now']; ?></button>
  <script type="text/javascript">
$(document).ready(function (e) {
	$("#free").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "<?php echo $setting['website_url'];?>/system/assets/ajax/form-process-download.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			beforeSend: function() 
        {
            $("#result2").html('<hr><div class="alert alert-primary" role="alert"><i class="fas fa-circle-notch fa-spin"></i> Verifying Request...</div>');
        },  
        success: function(response)
        {
            $("#result2").html(response);
        }        
	   });
	}));
});
</script>
  <div id="result2"></div>
</form><?php } elseif($details['free']=='1'){?>
<div class="alert alert-primary" role="alert">
  <?php echo $l['loggedinaccess']; ?>
</div>
<?php } else{} ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $l['support']; ?> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
     <?php echo ($details['support']=='1'?'<div class="alert alert-success" role="alert"> '.$l['we_do_supporti'].'</div>':'<div class="alert alert-danger" role="alert"> '.$l['do_not_support'].'</div>');?>      
 <?php if($user->is_loggedin() && $purchases->is_purchased($_SESSION['uid'],$id) && $details['support']=='1'){ ?>     <form id="support">
            <div class="form-group">
    <input type="hidden" class="form-control" name="name" placeholder="Enter Name" value="<?php echo $userDetails['fname']; ?>" ="">
  </div>
  <div class="form-group">
    <input type="hidden" name="email" class="form-control" value="<?php echo $userDetails['email']; ?>" ="">
  </div>
    <div class="form-group">
    <input type="hidden" name="subject" class="form-control" value="<?php echo $details['name']; ?>" ="">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><?php echo $l['message']; ?></label>
    <textarea name="message" class="form-control" id="exampleInputPassword1"></textarea>
  </div>
  <button type="submit" class="btn btn-primary submit"><?php echo $l['submit']; ?></button>
  <script type="text/javascript">
$(document).ready(function (e) {
	$("#support").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "<?php echo $setting['website_url'];?>/system/assets/ajax/form-process.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			beforeSend: function() 
        {
            $("#result").html('Verifying...');
        },  
        success: function(response)
        {
            $("#result").html(response);
        }        
	   });
	}));
});
</script>
  <div id="result"></div>
</form><?php } elseif($details['support']=='1'){?>
<div class="alert alert-primary" role="alert">
  <?php echo $l['loggedin2supp']; ?>
</div>
<?php } else{} ?>
      </div>
    </div>
  </div>
</div>
<?php if($details['reviews_off'] == '0'){?>
<!-- Modal -->
<div class="modal fade" id="exampleReview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $l['reviews']; ?> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 <?php if($user->is_loggedin() && $purchases->is_purchased($_SESSION['uid'],$id) && !$product->is_alreadyadd($_SESSION['uid'],$id)){ ?>  
 
 <form id="review">
            <div class="form-group">
    <input type="hidden" class="form-control" name="name" placeholder="Enter Name" value="<?php echo $userDetails['fname']; ?>" ="">
  </div>
  <div class="form-group">
    <input type="hidden" name="email" class="form-control" value="<?php echo $userDetails['email']; ?>" ="">
  </div>
    <div class="form-group">
    <input type="hidden" name="subject" class="form-control" value="<?php echo $details['name']; ?>" ="">
  </div>
      <input type="hidden" class="form-control" name="uid" value="<?php echo $_SESSION['uid']; ?>">
      <input type="hidden" class="form-control" name="pid" value="<?php echo $details['id']; ?>">
      
  <input type="radio" name="rating"
      value="5" /> 5 <input type="radio" name="rating" value="4" /> 4
      <input type="radio" name="rating" value="3" /> 3 <input type="radio"
      name="rating" value="2" /> 2 <input type="radio" name="rating" value="1" /> 1
  
  <div class="form-group">
    <label for="exampleInputPassword1"><?php echo $l['review']; ?></label>
    <textarea name="review" class="form-control" id="exampleInputPassword1"></textarea>
  </div>
  <button type="submit" class="btn btn-primary submit"><?php echo $l['submit']; ?></button>
  <script type="text/javascript">
$(document).ready(function (e) {
	$("#review").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "<?php echo $setting['website_url'];?>/system/assets/ajax/form-process-review.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			beforeSend: function() 
        {
            $("#result1").html('Verifying...');
        },  
        success: function(response)
        {
            $("#result1").html(response);
        }        
	   });
	}));
});
</script>

  <div id="result1"></div>
</form><?php } else{?>
<div class="alert alert-primary" role="alert">
  <?php echo $l['loggedin2rev']; ?>
</div>
<?php } ?>

 <?php if($user->is_loggedin() && $product->is_alreadyadd($_SESSION['uid'],$id)){ ?>
    <div class="alert alert-danger mb-0" role="alert"><?php echo $l['alreadyleftrev']; ?></div>
<?php } ?>

      <br>
      <h4 class="text-center mb-4 font-weight-bold"><?php echo $l['all_rev']; ?></h4>
      <?php     
      $num = $product->countReview($id);
           if($num > 0){ ?>
      <ul class="list-group text-center list-group-flush">
           <?php

      foreach( $allreviews as $row ) {
        $uname = $user->details($row['user_id']);
        $uname = $uname['username'];
          ?>
          
      <li class="list-group-item">
        <a href="<?php echo $setting['website_url'];?>/profile/<?php echo $uname; ?>/"><b><?php echo $uname; ?></b></a> has given <strong><?php echo $row['rating']; ?> Stars</strong> and said <strong>"<?php echo $row['review']; ?>"</strong> 
      <?php if($user->is_loggedin() && $row['user_id']==$_SESSION['uid']){ ?>
<a href="<?php echo $setting['website_url']; ?>/item.php?id=<?php echo $id;?>&del=<?php echo $row['id']; ?>" class="btn-style-edit" name="delete">Delete</a>
<?php } ?>
      <?php if($user->is_loggedin() && $userDetails['moderator'] == 1){ ?>
<a href="<?php echo $setting['website_url']; ?>/item.php?id=<?php echo $id;?>&delp=<?php echo $row['id']; ?>" class="btn btn-primary btn-sm" title="Staff Moderator Delete" name="delete"><i class="fas fa-trash-alt"></i></a>
<?php } ?>
</li>
      
      <?php } ?>
      </ul>
<?php } else { echo '<div class="alert alert-primary" role="alert"> <i class="far fa-frown"></i> '.$l['no-rev'].'</div>'; } ?>
      </div>
    </div>
  </div>
</div><?php } else{ } ?>  

    </main>
<?php
require_once('system/assets/footer.php');
?>
 <?php
}else{
    header('Location:index.php');
}

function display_post_not_found($id) {
    echo "Product with ID:$id does not exist!";
}