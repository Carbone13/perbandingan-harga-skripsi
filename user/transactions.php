<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$pageTitle ='Transactions';
$pg ='4';
require_once('../system/config-user.php');
$num  = $transaction->countAll('user_id',$crypt->decrypt($_SESSION['uid'],'USER'));
$currpage = (isset($_GET['page'])) ? $_GET['page'] : 1;
$maxres = 15;
$pages = $num / $maxres;
$pages = ceil($pages);
$start = ( $currpage - 1 ) * $maxres ;
$last = $start + $maxres -1;
$transactions = $transaction->getUserTransactions($crypt->decrypt($_SESSION['uid'],'USER'),$start,$maxres);
require_once('includes/header.php');
if(isset($_GET['type']) && isset($_GET['msg'])){
echo  "<div class=\"alert mb-0 mt-3 ".$_GET['type']."\" style=\"display:block;\">".$_GET['msg']."<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>";
}?>
<div class="row">
  <div class="col-4">
  
      <div class="my-3 p-3 bg-white rounded box-shadow">
      
<?php include'includes/sidenav.php';?>

</div>

</div>
  <div class="col-8 pl-0">
<?php if($num > 0){ 
?>

         <div class="my-3 p-3 bg-white rounded box-shadow">   


  <div id="transactions" class="collapse show">

     <div class="table-responsive">
      <table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col"><?php echo $l['id'];?></th>
      <th scope="col"><?php echo $l['date'];?></th>
      <th scope="col"><?php echo $l['type'];?></th>
      <th scope="col"><?php echo $l['details'];?></th>
      <th scope="col"><?php echo $l['amount'];?></th>
      <th scope="col"><?php echo $l['status'];?></th>
    </tr>
  </thead>
  <tbody>
  <?php
foreach($transactions as $trans){
if($trans['product']!='0'){
$prod = $product->details($trans['product']);
$prod = $prod['name'];
}else{
$prod = 'Purchase Credits';
}
if($trans['type']=='3'){
$prod = 'Free Credits';
}
?>
    <tr>
      <th scope="row"><?php echo $trans['id'];?></th>
      <td><?php echo $trans['date'];?></td>
      <td><?php echo ($trans['type']=='1'?'Item Purchase':'Credit Deposit');?></td>
      <td><?php echo $prod;?></td>
      <td><?php echo $setting['currency_sym'] . $trans['amount'];?></td>
            <td><?php echo ($trans['callback']=='1'?'<span data-toggle="tooltip" data-placement="top" title="Completed" class="badge badge-primary badge-pill"><i class="fa fa-check" aria-hidden="true"></i></span>':'<span data-toggle="tooltip" data-placement="top" title="Failed" class="badge badge-primary badge-pill"><i class="fa fa-close" aria-hidden="true"></i></span>');?></td>
    </tr>
<?php
}
?>
  </tbody>
</table>
   </div>

  </div>
     
      </div>


<br>
  <ul class="pagination justify-content-center">
<?php 
$back = (($currpage == 1)? '#':'transactions.php?page='.($currpage-1));
$next = (($currpage == $pages)? 'transactions.php?page='.$currpage:'transactions.php?page='.($currpage+1));
?>
    <li class="page-item">
      <a class="page-link" <?php echo ($currpage == 1)?"class='disabled'":''; ?> data-toggle="tooltip" data-placement="top" title="Previous" href="<?php echo $back;?>" tabindex="-1"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    </li>
    <li class="page-item">
      <a class="page-link" <?php echo ($currpage == $pages)?"class='disabled'":''; ?> data-toggle="tooltip" data-placement="top" title="Next" href="<?php echo $next;?>"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
    </li>
  </ul>
  <?php
 }else{
 echo  '<div class="my-3 p-3 bg-white rounded box-shadow"><div class="alert alert-primary mb-0">'.$l['notransfound'].'</div></div>';
 } ?>
  </div>
<?php require_once('includes/footer.php');
?>