<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"  type="text/css">
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $setting['website_url'];?>/system/assets/css/styles.css">
<title><?php echo $pageTitle . " | " . $setting['site_name']; ?></title>

<script>
$( document ).ready(function() {
$('.js-open').on('click', function() {
  var target = $(this).attr('data-target'); 
  $(target).toggleClass('is-visible');  
});

$('.js-close').on('click', function() {
  $(this).parent().removeClass('is-visible');
});

    $('[data-toggle="tooltip"]').tooltip();

});
</script>
</head>
<body>

<div id="js-favourites" class="off-canvas-panel">
  <button class="btn js-close btn-close btn-sm btn-outline-primary">Close</button>
  <div class="heading">    
   <a class="text-dark" href="<?php echo $setting['website_url'];?>"><?php if(empty($setting['site_logo'])){ ?><?php echo $setting['site_name'];?><?php } else{?><img src="<?php echo $setting['website_url']."/system/assets/uploads/img/".$setting['site_logo']; ?>" width="" height="30"><?php } ?></a>
  </div>
  <div class="inner">    

  <ul class="list-group list-group-flush">
  <li class="list-group-item"><a href="<?php echo $setting['website_url'];?>">Store</a></li>
  <li class="list-group-item">About</li>
  <li class="list-group-item">Contact Us</li>
  <li class="list-group-item">News</li>
</ul>

  </div>
    <a class="btn btn-primary btn-lg" href="<?php echo $setting['website_url'];?>/user/login.php?logout">Sign Out</a>
   
</div>


  <header class="blog-header py-3 bg-white">
  <div class="container">
        <div class="row flex-nowrap justify-content-between align-items-center">
          <div class="col-4 pt-0">
            <a class="text-muted js-open" data-target="#js-favourites" aria-controls="js-favourites"><i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
          <div class="col-4 text-center">
            <a class="text-dark" href="<?php echo $setting['website_url'];?>"><?php if(empty($setting['site_logo'])){ ?><?php echo $setting['site_name'];?><?php } else{?><img src="<?php echo $setting['website_url']."/system/assets/uploads/img/".$setting['site_logo']; ?>" width="" height="30"><?php } ?></a>
          </div>
          <div class="col-4 d-flex justify-content-end align-items-center">
 <?php if($user->is_loggedin()){ ?>
<div class="dropdown">
  <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Hi, <?php echo $userDetails['fname'];?>
  </button>
  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="<?php echo $setting['website_url'];?>/user/">Overview <span class="badge badge-pill badge-primary"><?php echo $setting['currency_sym'] .  $userDetails['balance'];?></span></a>
    <a class="dropdown-item" href="<?php echo $setting['website_url'];?>/user/downloads.php">Downloads</a>
       <a href="<?php echo $setting['website_url'];?>/user/news.php" class="dropdown-item">News</a>
    <a class="dropdown-item" href="<?php echo $setting['website_url'];?>/user/login.php?logout">Sign Out</a>
  </div>
</div> <?php } else{?>

  <button class="btn btn-sm btn-outline-primary" type="button">
    Login
  </button>
  
  <?php } ?>

          </div>
        </div>
        </div>
      </header>
 
 <?php if($user->is_loggedin()){ ?>
    <div class="nav-scroller bg-white box-shadow justify-content-md-center">
      <nav class="nav nav-underline justify-content-md-center">
        <a class="nav-link <?php echo $pg == '1' ? 'active':NULL ?>" href="<?php echo $setting['website_url'];?>/user/">Overview</a>
          <a href="<?php echo $setting['website_url'];?>/user/news.php" class="nav-link <?php echo $pg == '7' ? 'active':NULL ?>">News</a>
        <a class="nav-link <?php echo $pg == '2' ? 'active':NULL ?>" href="<?php echo $setting['website_url'];?>/user/downloads.php">Downloads</a>
        <a class="nav-link <?php echo $pg == '3' ? 'active':NULL ?>" href="<?php echo $setting['website_url'];?>/user/credits.php">Buy Credits</a>
        <a class="nav-link <?php echo $pg == '4' ? 'active':NULL ?>" href="<?php echo $setting['website_url'];?>/user/transactions.php">Transactions</a>
        <a class="nav-link <?php echo $pg == '5' ? 'active':NULL ?>" href="<?php echo $setting['website_url'];?>/user/wishlist.php">Wishlist <span class="badge badge-pill badge-primary align-text-bottom"><?php echo $wishcount;?></span></a>
        <a class="nav-link <?php echo $pg == '6' ? 'active':NULL ?>" href="<?php echo $setting['website_url'];?>/user/account.php">My Account</a>
      </nav>
    </div>   <?php } ?>

<div class="container"><div class="jumbotron p-3 p-md-5 mt-3 mb-0 text-white rounded bg-dark box-shadow">
        <div class="col-md-12 px-0">
          <h1 class="display-4 text-center"><?php echo $pageTitle;?></h1>
        </div>
      </div></div>
      
         <main role="main" class="container">