<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pageTitle ='Overview';
$pg ='1';
require_once('../system/config-user.php');
require_once('includes/header.php');
$num  = $transaction->countAll('user_id',$crypt->decrypt($_SESSION['uid'],'USER'));
$currpage = (isset($_GET['page'])) ? $_GET['page'] : 1;
$maxres = 5;
$pages = $num / $maxres;
$pages = ceil($pages);
$start = ( $currpage - 1 ) * $maxres ;
$last = $start + $maxres -1;
$transactions = $transaction->getUserTransactions($crypt->decrypt($_SESSION['uid'],'USER'),$start,$maxres);
?>


   
<div class="mt-3 p-3 bg-white rounded box-shadow">
<div class="row">
        <div class="col">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-money"></i>
            <div class="info">
              <h4>Current Credits</h4>
              <h5><b><span class="badge badge-pill badge-primary"><?php echo $setting['currency_sym'] . $userDetails['balance'];?></span></b></h5>
            </div>
          </div>
        </div>
                <div class="col">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-gift"></i>
            <div class="info">
              <h4>Wishlist Items</h4>
              <h5><b><span class="badge badge-pill badge-primary"><?php echo $wishcount;?></span></b></h5>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-shopping-cart"></i>
            <div class="info">
              <h4>Total Purchases</h4>
              <h5><b><span class="badge badge-pill badge-primary"><?php echo $userDetails['purchases'];?></span></b></h5>
            </div>
          </div>
        </div>
      </div>
   </div>

<div class="row">
  <div class="col-4">
  
      <div class="my-3 p-3 bg-white rounded box-shadow">
  
<?php include'includes/sidenav.php';?>

                  <div class="card mt-3">
                    <div class="card-body">
                        <h5><i class="fa fa-trophy mr-3"></i>Achievements<br></h5>
                        <hr>
                        <ul class="list-inline">
                           <?php if($userDetails['active'] == 1){?> <li class="list-inline-item"><i class="fa fa-certificate" aria-hidden="true"></i> Pioneer</li><?php } ?>
                           <?php if($userDetails['verified'] == 1){?> <li class="list-inline-item"><i class="fa fa-user-md" aria-hidden="true"></i> Became an Author</li><?php } ?>
                           <?php if($userDetails['allow_email'] == 1){?> <li class="list-inline-item"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Newsletter Squad</li><?php } ?>
                           <?php if($userDetails['moderator'] == 1){?> <li class="list-inline-item"><i class="fa fa-user-secret" aria-hidden="true"></i> Staff Moderator</li><?php } ?>
                           <?php if($userDetails['purchases'] >= 1 && $userDetails['purchases'] <= 10){?> <li class="list-inline-item"><i class="fa fa-cart-plus" aria-hidden="true"></i> Buyer Level 1</li><?php } ?>
                           <?php if($userDetails['purchases'] >= 11 && $userDetails['purchases'] <= 30){?> <li class="list-inline-item"><i class="fa fa-cart-plus" aria-hidden="true"></i> Buyer Level 2</li><?php } ?>
                           <?php if($userDetails['purchases'] >= 31 && $userDetails['purchases'] <= 100){?> <li class="list-inline-item"><i class="fa fa-cart-plus" aria-hidden="true"></i> Buyer Level 3 (max level)</li><?php } ?>
                        </ul>
                    </div>
                </div>  

</div>

</div>
    <div class="col-8 pl-0">

         <div class="my-3 p-3 bg-white rounded box-shadow">   
      <h4 class="pb-2 mb-2">Recent Transactions</h4>

  <div id="transactions" class="collapse show">
 
     <div class="table-responsive">
      <table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Date</th>
      <th scope="col">Details</th>
      <th scope="col">Amount</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  <?php
foreach($transactions as $trans){
if($trans['product']!='0'){
$prod = $product->details($trans['product']);
$prod = $prod['name'];
}else{
$prod = 'Purchase Credits';
}
if($trans['type']=='3'){
$prod = 'Free Credits';
}
?>
    <tr>
      <th scope="row"><?php echo $trans['id'];?></th>
      <td><?php echo $trans['date'];?></td>
     <!-- <td><?php echo ($trans['type']=='1'?'Item Purchase':'Credit Deposit');?></td>-->
      <td><?php echo $prod;?></td>
      <td><?php echo $setting['currency_sym'] . $trans['amount'];?></td>
      <td><?php echo ($trans['callback']=='1'?'<span data-toggle="tooltip" data-placement="top" title="Completed" class="badge badge-primary badge-pill"><i class="fa fa-check" aria-hidden="true"></i></span>':'<span data-toggle="tooltip" data-placement="top" title="Failed" class="badge badge-primary badge-pill"><i class="fa fa-close" aria-hidden="true"></i></span>');?></td>
    </tr>
<?php
}
?>
  </tbody>
</table>
   </div>

									<div class="row justify-content-md-center">
										<a href="<?php echo $setting['website_url'];?>/user/transactions.php" class="btn btn-sm btn-outline-primary">View All Transactions</a>
									</div>

  </div>
     
      </div>
      
   

</div> 
</div> 
   

<?php
require_once('includes/footer.php');
?>