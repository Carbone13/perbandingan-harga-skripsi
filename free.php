<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pageTitle = 'Free Files of the Week';

require_once('system/config-global.php');

require_once('system/assets/header.php');

?>
  <main role="main">

      
        <!-- Masthead -->
    <header class="masthead text-white text-center mb-3 mt-0 rounded-0 box-shadow">
      <div class="overlay rounded-0 box-shadow"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-1 font-weight-bold"><?php echo $l['limitedfree']?></h1>
            <h2><?php echo $l['downloadb4gone']?></h2>
          </div>
        </div>
      </div>
    </header>
<br>

<div class="container">
        <div class="row p-15">
            <div class="col-lg-12">

 <div class="row">
     <?php
     $free = $product->getFreeProducts();
      foreach( $free as $row ) {
          $str = $row['name'];
          ?>
          
              <div class="col-lg-4 col-md-4 col-sm-6 mb-4">
    <div class="card box-shadow h-100">
        <div class="relativel">
            <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>">
        <img class="card-img-top" src="<?php echo $setting['website_url']; ?>/system/assets/uploads/products/<?php echo $row['preview_img']?>" alt="<?php echo $row['name']?>"></a>
        </div>
      <div class="card-body text-center">
          <h5 class="card-title mb-3"><?php echo $row['name']?></h5>
         <?php if($setting['show_card_sde'] == '1'){?> <p class="card-text"><?php echo $row['short_des']?></p><?php } ?>
    <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>" class="btn btn-sm btn-outline-secondary box-shadow"><?php echo $l['view_product']?></a>
      </div>
          <div class="card-footer">
<div class="clearfix">
 <?php if($row['views_off'] == '0'){?><a class="btn btn-sm float-left pr-0"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $row['views']; ?></a><?php } else{ } ?>
  <?php if($row['price'] == 0){ ?>
      <button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } elseif($row['free'] == 1){ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><s><?php echo $setting['currency_sym'].$row['price'];?></s></button><button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } else{ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $setting['currency_sym'].$row['price'];?></button>
  <?php } ?>
</div>
    </div>
    </div>
  </div>
          
      <?php } ?>
    </div>  

            </div>
            <!-- /.col-lg-9 -->
        </div>
            </div>
    

    </main>
<?php
require_once('system/assets/footer.php');
?>