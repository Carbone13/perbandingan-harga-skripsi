<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pageTitle ='Perbandingan Harga Marketplace Indonesia - Index';
require_once('system/config-global.php');
require_once('system/assets/header.php');

$featuredp = $product->getFeaturedProducts();
$newp = $product->getNewProducts();
$popp = $product->getPopularProducts();
$free = $product->getFreeProducts();
?>
  <main role="main">
    <!-- Masthead -->
    <header class="masthead text-white text-center mb-0 mt-0 rounded-0 box-shadow">
      <div class="overlay rounded-0 box-shadow"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-1 font-weight-bold"><?php echo $setting['homepage_header']; ?></h1>
            <h5 class="mb-4 font-weight-bold"><?php echo $setting['homepage_subheader']; ?></h5>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            
<div class="subcribe-form" id="mc_embed_signup">

								<form action="search.php" method="GET" class="subscription relative">
										<input placeholder="<?php echo $l['searchplaceholder']?>" name="key" minlength="3" pattern=".{3,}" required="" type="text" class="form-control input-sm">
										<button class="primary-btn btn align-items-center"><span class="mr-10"><?php echo $l['search']?></span></button>
										
								</form>
							</div>
            
          </div>
        </div>
      </div>
    </header>
    
    <?php if(!$user->is_loggedin()){ ?>
    <div class="bg-dark1 jumbotron text-white text-center mb-4 p-4 rounded-0">
	<div class="container "><span class=" h4 link-info "><a class="font-weight-bold" href="<?php echo $setting['website_url'];?>/user/register.php">Buat akun sekarang</a> dan <strong>Bandingkan barang kesukaan anda</strong> dengan Murah	</span></div>
	</div>
	<?php } ?>
    
<br>
<div class="container">

    <div class="wrapper mb-3">
	          <span class="font-weight-bold h4 m-t-sm"><?php echo $l['ffotw']?></span>
		</div>
 <div class="row">
     <?php
      foreach( $free as $row ) {
          $str = $row['name'];
          ?>
          
              <div class="col-lg-4 col-md-4 col-sm-6 mb-4">
    <div class="card box-shadow h-100">
        <div class="relativel">
            <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>">
                 <?php if($row['featured'] == 1){ ?>
            <div class="typebadge"><span class="bg-primary"><i class="fa fa-file mr-1" aria-hidden="true"></i><?php echo $l['featured']?></span></div>
            <?php } ?>
        <img class="card-img-top" src="<?php echo $row['gambar_produk']?>" alt="<?php echo $row['name']?>"></a>
        </div>
      <div class="card-body text-center">
          <h5 class="card-title mb-3"><?php echo $row['name']?></h5>
         <?php if($setting['show_card_sde'] == '1'){?> <p class="card-text"><?php echo $row['short_des']?></p><?php } ?>
    <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>" class="btn btn-sm btn-outline-secondary box-shadow"><?php echo $l['view_product']?></a>
      </div>
          <div class="card-footer">
<div class="clearfix">
 <?php if($row['views_off'] == '0'){?><a class="btn btn-sm float-left pr-0"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $row['views']; ?></a><?php } else{ } ?>
 <a class="btn btn-sm float-left"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo $row['sales'];?></a>
  <?php if($row['price'] == 0){ ?>
      <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } elseif($row['free'] == 1){ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><s><?php echo $setting['currency_sym'].$row['price'];?></s></button><button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } else{ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $setting['currency_sym'].$row['price'];?></button>
  <?php } ?>
</div>
    </div>
    </div>
  </div>
          
      <?php } ?>
    </div>  

<div class="wrapper mb-3 mt-3">
	          <span class="font-weight-bold h4 m-t-sm"><?php echo $l['featured_items']?></span>
		</div>
 <div class="row">
     <?php
      foreach( $featuredp as $row ) {
          $str = $row['name'];
          ?>
          
              <div class="col-lg-4 col-md-4 col-sm-6 mb-4">
    <div class="card box-shadow h-100">
        <div class="relativel">
            <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>">
                 <?php if($row['featured'] == 1){ ?>
            <div class="typebadge"><span class="bg-primary"><i class="fa fa-file mr-1" aria-hidden="true"></i><?php echo $l['featured']?></span></div>
            <?php } ?>
        <img class="card-img-top" src="<?php echo $row['gambar_produk']?>" alt="<?php echo $row['name']?>"></a>
        </div>
      <div class="card-body text-center">
          <h5 class="card-title mb-3"><?php echo $row['name']?></h5>
         <?php if($setting['show_card_sde'] == '1'){?> <p class="card-text"><?php echo $row['short_des']?></p><?php } ?>
    <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>" class="btn btn-sm btn-outline-secondary box-shadow"><?php echo $l['view_product']?></a>
      </div>
          <div class="card-footer">
<div class="clearfix">
  <?php if($row['views_off'] == '0'){?><a class="btn btn-sm float-left pr-0"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $row['views']; ?></a><?php } else{ } ?>
 <a class="btn btn-sm float-left"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo $row['sales'];?></a>
  <?php if($row['price'] == 0){ ?>
      <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } elseif($row['free'] == 1){ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><s><?php echo $setting['currency_sym'].$row['price'];?></s></button><button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } else{ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $setting['currency_sym'].$row['price'];?></button>
  <?php } ?>
</div>
    </div>
    </div>
  </div>
          
      <?php } ?>
    </div>  

<div class="wrapper mb-3 mt-3">
	          <span class="font-weight-bold h4 m-t-sm"><?php echo $l['popular_items']?></span>
		</div>
       <div class="row">
           <?php
      foreach( $popp as $row ) {
          $str = $row['name'];
          ?>
          
              <div class="col-sm-4 mb-4">
    <div class="card box-shadow h-100">
        <div class="relativel">
            <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>">
                 <?php if($row['featured'] == 1){ ?>
            <div class="typebadge"><span class="bg-primary"><i class="fa fa-file mr-1" aria-hidden="true"></i><?php echo $l['featured']?></span></div>
            <?php } else{ ?>
             <div class="typebadge"><span class="bg-primary"><i class="fa fa-fire mr-1" aria-hidden="true"></i><?php echo $l['popular']?></span></div>
            <?php } ?>
        <img class="card-img-top" src="<?php echo $row['gambar_produk']?>" alt="<?php echo $row['name']?>"></a>
        </div>
      <div class="card-body text-center">
          <h5 class="card-title mb-3"><?php echo $row['name']?></h5>
         <?php if($setting['show_card_sde'] == '1'){?> <p class="card-text"><?php echo $row['short_des']?></p><?php } ?>
    <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>" class="btn btn-sm btn-outline-secondary box-shadow"><?php echo $l['view_product']?></a>
      </div>
          <div class="card-footer">
<div class="clearfix">
 <?php if($row['views_off'] == '0'){?><a class="btn btn-sm float-left pr-0"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $row['views']; ?></a><?php } else{ } ?>
 <a class="btn btn-sm float-left"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo $row['sales'];?></a>
  <?php if($row['price'] == 0){ ?>
      <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } elseif($row['free'] == 1){ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><s><?php echo $setting['currency_sym'].$row['price'];?></s></button><button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } else{ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $setting['currency_sym'].$row['price'];?></button>
  <?php } ?>
</div>
    </div>
    </div>
  </div>
          
      <?php } ?>
</div> 
      
<div class="wrapper mb-3 mt-3">
	          <span class="font-weight-bold h4 m-t-sm"><?php echo $l['new_items']?></span>
		</div>
       <div class="row">
           <?php
      foreach( $newp as $row ) {
          $str = $row['name'];
          ?>
          
              <div class="col-sm-4 mb-4">
              <div class="card box-shadow h-100">
        <div class="relativel">
            <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>">
 <div class="typebadge"><span class="bg-primary"><i class="fa fa-star mr-1" aria-hidden="true"></i><?php echo $l['new']?></span></div>
        <img class="card-img-top" src="<?php echo $row['gambar_produk']?>" alt="<?php echo $row['name']?>"></a>
        </div>
      <div class="card-body text-center">
          <h5 class="card-title mb-3"><?php echo $row['name']?></h5>
         <?php if($setting['show_card_sde'] == '1'){?> <p class="card-text"><?php echo $row['short_des']?></p><?php } ?>
    <a href="<?php echo $setting['website_url']; ?>/item/<?php echo $row['id']?>/<?php echo wordwrap(strtolower($str), 1, '-', 0); ?>" class="btn btn-sm btn-outline-secondary box-shadow"><?php echo $l['view_product']?></a>
      </div>
          <div class="card-footer">
<div class="clearfix">
 <?php if($row['views_off'] == '0'){?><a class="btn btn-sm float-left pr-0"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $row['views']; ?></a><?php } else{ } ?>
 <a class="btn btn-sm float-left"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo $row['sales'];?></a>
  <?php if($row['price'] == 0){ ?>
      <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } elseif($row['free'] == 1){ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><s><?php echo $setting['currency_sym'].$row['price'];?></s></button><button type="button" class="btn btn-success btn-sm float-right font-weight-bold"><?php echo $l['free']?></button>
  <?php } else{ ?>
  <button type="button" class="btn btn-primary btn-sm float-right font-weight-bold"><?php echo $setting['currency_sym'].$row['price'];?></button>
  <?php } ?>
</div>
    </div>
    </div>
  </div>
          
      <?php } ?>
</div> 

    </main>
<?php
require_once('system/assets/footer.php');
?>