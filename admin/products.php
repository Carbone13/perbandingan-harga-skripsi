<?php 

$pageTitle = "Produk";
require_once('../system/config-admin.php');

if(isset($_REQUEST['path'])){
		$file = $_REQUEST['path'];
		if (!@fopen($file,'r') && (!@file_exists($file)) ){
		echo 'File not found, Check file path';
		print_r(error_get_last());
		exit;
		}else{
		echo 'success';
		exit;
		}
}

$currpage = (isset($_GET['page'])) ? $_GET['page'] : 1;
$maxres = 20;
$num = $product->countAll();;
$pages = $num / $maxres;
$pages = ceil($pages);
$start = ( $currpage - 1 ) * $maxres ;
$last = $start + $maxres -1;
$allProducts = $product->getProducts($start,$maxres);
require_once('includes/header1.php');
?>
                        <div class="content">
                        
<nav class="navbar navbar-expand-lg navbar-dark text-white rounded bg-primary box-shadow">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/products.php">Seluruh Produk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/add-product.php">Tambah Produk</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/best-selling-products.php">Produk Terbanyak Di Kunjungi</a>
          </li>
        </ul>
      </div>
    </nav>
                        
                        <div class="my-3 p-3 bg-white rounded box-shadow">

<?php if($num > 0){ ?>

<table class="table table-hover table-striped table-bordered">
<thead>
<tr>
<th>Product Name</th>
<th>Product Price</th>
<th>Product Status</th>
<th>Views</th>
<th>Sales</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
<?php 
foreach($allProducts as $product) {
?>
<tr>
<td><?php echo $product['name'];?></td>
<td><?php echo $setting['currency_sym']." ".$product['price'];?></td>
<td><?php echo ($product['active']=='1'?'<span data-toggle="tooltip" data-placement="top" title="Item Active" class="badge badge-primary badge-pill">Active</span>':'<span data-toggle="tooltip" data-placement="top" title="Item Paused" class="badge badge-primary badge-pill">Paused</span>');?></td>
<td><?php echo $product['views'];?></td>
<td><?php echo $product['sales'];?></td>
<td><div class="btn-group btn-group-sm" role="group" aria-label="AActions"><a href="<?php echo $setting['website_url']; ?>/item.php?id=<?php echo $product['id']; ?>" class="btn btn-outline-primary">View</a><a href="edit-product.php?id=<?php echo $product['id']; ?>" class="btn btn-outline-primary">Edit</a><a href="remove-product.php?id=<?php echo $product['id']; ?>" class="btn btn-outline-primary">Delete</a></div></td>
</tr>
<?php }?>
</tbody></table></div>

<br>
  <ul class="pagination justify-content-center">
<?php 
$back = (($currpage == 1)? '#':'products.php?page='.($currpage-1));
$next = (($currpage == $pages)? 'products.php?page='.$currpage:'products.php?page='.($currpage+1));
?>
    <li class="page-item">
      <a class="page-link" <?php echo ($currpage == 1)?"class='disabled'":''; ?> data-toggle="tooltip" data-placement="top" title="Previous" href="<?php echo $back;?>" tabindex="-1"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
    </li>
    <li class="page-item">
      <a class="page-link" <?php echo ($currpage == $pages)?"class='disabled'":''; ?> data-toggle="tooltip" data-placement="top" title="Next" href="<?php echo $next;?>"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
    </li>
  </ul>
</div></div>
 <?php 
 }else{
 echo  "<div class='alert'>Belum terdapat produk</div>";
 }
require_once('includes/footer.php');
?>