<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
require_once('../system/config-admin.php');
if(!preg_match('/^([1-9][0-9]*|0)(\.[0-9]{2})?$/', $_POST['price']))
{
  echo '<span class="text-danger">You need to put a price</span>';
  die();
}
if(empty($_POST['name']))
{
  echo '<span class="text-danger">Product Name is required!</span>';
  die();
}
if(empty($_POST['sdesc']))
{
  echo '<span class="text-danger">Short Description is required.</span>';
  die();
}
if(empty($_POST['description']))
{
  echo '<span class="text-danger">Description is required.</span>';
  die();
}
if(empty($_POST['cat_id']))
{
  echo '<span class="text-danger">Category is required.</span>';
  die();
}

$id = $_POST['id'];
$name2 = $_POST['name'];
$sdesc = $_POST['sdesc'];
$description = $_POST['description'];
$cat_id = $_POST['cat_id'];
//$scat_id = $_POST['subcat'];
$scat_id = (isset($_POST['subcat']) ? $_POST['subcat'] : null);
$demo = $_POST['demo'];
$price = $_POST['price'];
$date = date("Y-m-d");
$featured = (isset($_POST['featured'])) ? 1 : 0;
$support = (isset($_POST['support'])) ? 1 : 0;
$stock_on = (isset($_POST['stock_on'])) ? 1 : 0;
$stock = $_POST['stock'];
$views_off = (isset($_POST['views_off'])) ? 1 : 0;
$reviews_off = (isset($_POST['reviews_off'])) ? 1 : 0;
$free = (isset($_POST['free'])) ? 1 : 0;
//$tags = $_POST['tags'];
$active = $_POST['active'];

$sql_upload = $DB_con->prepare("UPDATE dsptesty_products SET name=:name2, short_des=:sdesc, description=:description, cat_id=:cat_id, subc_id=:scat_id, price=:price, demo=:demo, modified=:modified, featured=:featured, support=:support, active=:active, stock_on=:stock_on, stock=:stock, views_off=:views_off, reviews_off=:reviews_off, free=:free WHERE id=:id");

$sql_upload->bindparam(":name2",$name2);
$sql_upload->bindparam(":sdesc",$sdesc);
$sql_upload->bindparam(":description",$description);
$sql_upload->bindparam(":cat_id",$cat_id);
$sql_upload->bindparam(":scat_id",$scat_id);
$sql_upload->bindparam(":price",$price);
$sql_upload->bindparam(":demo",$demo);
$sql_upload->bindparam(":modified",$date);
$sql_upload->bindparam(":featured",$featured);
$sql_upload->bindparam(":support",$support);
$sql_upload->bindparam(":active",$active);
$sql_upload->bindparam(":stock_on",$stock_on);
$sql_upload->bindparam(":stock",$stock);
$sql_upload->bindparam(":views_off",$views_off);
$sql_upload->bindparam(":reviews_off",$reviews_off);
$sql_upload->bindparam(":free",$free);
//$sql_upload->bindparam(":tags",$tags);
$sql_upload->bindparam(":id",$id);

if($sql_upload->execute()){
echo '<span class="text-success">Product Updated!</span>';
}
else{
echo "Error: " . $sql_upload->error;
}

}
else {
  header('location: ../index.php');
}
?>