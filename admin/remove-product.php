<?php 

$pageTitle = "Remove Product";
require_once('../system/config-admin.php');
if(isset($_REQUEST['id'])){
$productDetails = $product->details($_REQUEST['id']);
$error = ($product->error?$product->error:false);

if(isset($_GET['action']) && $_GET['action'] == 'remove'){

if(!$error){
$product->remove($_REQUEST['id']);
$error = ($product->error?$product->error:false);
$success =($product->msg?$product->msg:false);
}
if(isset($_REQUEST['ajax'])){
echo (@$error ? $error:'success');
exit;
}

}

if(empty($error)){
unset($error);
}

}else{
require_once('includes/header1.php');
echo 'Invalid request';
require_once('includes/footer.php');
exit;
}
require_once('includes/header1.php');
?>
                        <div class="content">
                        
<nav class="navbar navbar-expand-lg navbar-dark text-white rounded bg-primary box-shadow">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/products.php">All Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/add-product.php">Add Product</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/best-selling-products.php">Best Sellers</a>
          </li>
        </ul>
      </div>
    </nav>
                        
                        <div class="my-3 p-3 bg-white rounded box-shadow">
 <?php if(!isset($error) || !isset($success)){?>
<form class="form-horizontal" id="productRemove" method="POST" action="remove-product.php?action=remove">
  
   <input class="form-control" type="hidden" name="id" value="<?php echo $productDetails['id']; ?>">
   <h4>Are you sure you want to remove <?php echo $productDetails['name']; ?> ?</h4>
<div class="form-actions">
  <button type="submit" class="btn btn-danger">Remove Product</button>
</div>
</form>
 <?php }
require_once('includes/footer.php');
?>