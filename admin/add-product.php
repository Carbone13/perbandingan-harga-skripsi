<?php 

$pageTitle = "Tambah Produk";
require_once('../system/config-admin.php');

$category = $product->get_categories();
//$category1 = $product->get_subcategories();
require_once('includes/header1.php');
require_once('includes/function.php');
?>
      <script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.5/tinymce.min.js"></script>
                        <div class="content">
                        
<nav class="navbar navbar-expand-lg navbar-dark text-white rounded bg-primary box-shadow">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/products.php">Seluruh Produk</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/add-product.php">Tambah Produk</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo $setting['website_url'];?>/admin/best-selling-products.php">Produk Terbanyak Di Kunjungi</a>
          </li>
        </ul>
      </div>
    </nav>
                        
                        <div class="my-3 p-3 bg-white rounded box-shadow">
                            
                    
                          
                          <?php
                          
  $scrape_url = '';
  $hasil[0]['produk_nama'] = '';
  $hasil[0]['produk_deskripsi'] = '';
  $hasil[0]['harga_produk'] = '';
  $hasil[0]['seller_info'] = '';
  $hasil[0]['gambar_produk'] = '';
  if(isset($_POST['submit']) && !empty($_POST['scrape_url'])){
  $scrape_url = $_POST['scrape_url'];

        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, "https://php-1-codeanywhere565467.codeanyapp.com/scraper-tokopedia.php?url=$scrape_url"); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      

    $hasil = json_decode($output,true);
    //print_r($hasil);
    
  }
                          $deskripsi_singkat = '';
                          if($hasil[0]['produk_deskripsi'] !== ''){
                            $deskripsi_singkat = cano($hasil[0]['produk_deskripsi']).' ...';
                          }
                          
    ?>
                          
                                <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post" name="registerForm">
                            <div class="form-group"> <label>URL Produk:</label> <input class="form-control" name="scrape_url" id="coupon-code" type="text" value="<?php echo $scrape_url;?>"></div>
                            <input id="btn" class="btn btn-primary w-100" type="submit" name="submit" value="Scrape Produk" />
                            </form>
                          
                       
                            
                            <form id="upload" class="form-horizontal">
                              
                          
             <div class="form-group"> <label>Nama Produk:</label> <input class="form-control" name="name" id="coupon-code" type="text" value="<?php echo $hasil[0]['produk_nama'];?>"></div>
                              <div class="form-group"> <label>Nama Penjual:</label> <input class="form-control" name="seller_info" id="seller_info" type="text" value="<?php echo $hasil[0]['seller_info'];?>"></div>
             <div class="form-group"> <label>Deskripsi Singkat:</label> <input class="form-control" name="sdesc" id="coupon-code" type="text" value="<?php echo $deskripsi_singkat;?>"></div>
            <div class="form-group"> <label>Deskripsi:</label> <textarea type="text" class="form-control" name="description" id="coupon-code"><?php echo $hasil[0]['produk_deskripsi'];?></textarea></div><hr>
             <div class="form-group"> <label>Kategori:</label> <div class="input-group mb-3">
  <select class="custom-select" name="cat_id" id="cat_id" required>
    <option value="">Pilih Kategori...</option>
    <?php foreach($category as $cat) {
?>
    <option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
    <?php } ?>
  </select>
</div></div>
             <div class="form-group"> <label>Kategori Sub:</label> <div class="input-group mb-3">
  <select class="custom-select" name="subcat" id="subcat">
    <option value="">Pilih Subcategory...</option>

  </select>
</div></div>

<!--<select name="subcat" id="subcat">

</select>-->

<script type="text/javascript">
$(function() {
 
 $("#cat_id").bind("change", function() {
     $.ajax({
         type: "GET", 
         url: "ajax-category.php",
         data: "cat_id="+$("#cat_id").val(),
         success: function(html) {
             $("#subcat").html(html);
         }
     });
 });
            
 
});
</script>


<div class="form-group"> <label>Harga Produk Tokopedia:</label> <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text">Rp.</span>
  </div>
  <input type="number" name="price" class="form-control" aria-label="Amount" value="<?php echo $hasil[0]['harga_produk'];?>">
  <div class="input-group-append">
    <span class="input-group-text">,00</span>
  </div>
</div></div>
                              <?php $url_tokopedia = $uri = trim(strtok($scrape_url, '?')); ?>
               <div class="form-group"> <label>URL Tokopedia:</label> <input class="form-control" name="demo" id="coupon-code" type="url" value="<?php echo $url_tokopedia;?>"></div>
                               <div class="form-group"> <label>Gambar Utama Produk:</label> <input class="form-control" name="gambar_produk" id="gambar_produk" type="url" value="<?php echo $hasil[0]['gambar_produk'];?>"></div>
<!--TOKOPEDIA SELESAI PROSES-->         
<!--BUKALAPAK PROSES-->
                              <hr>

<div class="form-group"> <label>Harga Produk Bukalapak:</label> <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text">Rp.</span>
  </div>
  <input type="number" name="harga-bl" id="harga-bl" class="form-control" aria-label="Amount" value="">
  <div class="input-group-append">
    <span class="input-group-text">,00</span>
  </div>
</div></div>
                              
 <div class="form-group"> <label>URL Bukalapak:</label> <input class="form-control" name="url_bl" id="url-bl" type="url" value=""></div>


<div id='tunggu'></div>
<input class="btn btn-primary w-100" type='button' value='Scrape Bukalapak' id='postJson' />

 
<script type="text/javascript">

  $(document).ready(function(){
    $("#postJson").click(function(){
      $('#tunggu').html("<b>Mohon Tunggu , pengambilan data ..</b>");
  $.ajax({
  url: 'https://php-1-codeanywhere565467.codeanyapp.com/scraper-bukalapak.php?kw=<?php echo $hasil[0]['produk_nama'].'&harga='.$hasil[0]['harga_produk'];?>',
  dataType: 'json',
  success: function(data) {
    $( "#tunggu" ).remove();
    alert("Sukses Menambahkan Data Bukalapak");
    document.getElementById("url-bl").value = data.url_produk;
    
    document.getElementById("harga-bl").value = data.harga_produk;
    
  }
});
      });
    });
</script>
                              
              <!-- <div class="form-group"> <label>Tags (max 15):</label> <input class="form-control" name="tags[]" id="coupon-code" type="text"></div>-->
               
               <hr>
       
         <!--<div class="form-check">
    <input type="checkbox" name="featured" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Featured Item</label>
  </div>
           <div class="form-check">
    <input type="checkbox" name="free" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Make Free Item (Only download limited times)</label>
  </div>
    <div class="form-check">
    <input type="checkbox" name="support" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Support item</label>
  </div>
                                  <div class="form-check">
    <input type="checkbox" name="reviews_off" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Disable Reviews</label>
  </div>
                                  <div class="form-check">
    <input type="checkbox" name="views_off" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Hide View Counter</label>
  </div>
      <div class="form-check">
    <input type="checkbox" name="stock_on" id="exampleCheck1" data-toggle="collapse" data-target="#stockcoll" aria-expanded="false" aria-controls="stockcoll">
    <label class="form-check-label" for="exampleCheck1">Use Stock Limits</label>
  </div>-->
                              
                              <div class="collapse" id="stockcoll">
                                <br>
  <div class="card card-body">
   
    <div class="form-group"> <label>Stock Available:</label> <div class="input-group mb-0"><input class="form-control" value="0" name="stock" type="number"></div></div>
    
  </div>
</div>
       
       <hr>
                    <!--<div class="form-group"> <label>Main File (.ZIP):</label> 
                                   <div class="input-group">
  <div class="custom-file">
    <input type="file" name="mainfile" class="custom-file-input" id="inputGroupFile04">
    <label class="custom-file-label" for="inputGroupFile04">Choose File</label>
  </div>
</div>
                    </div>

                    <div class="form-group"> <label>Icon Image File (.PNG,.JPG):</label> 
                                   <div class="input-group">
  <div class="custom-file">
    <input type="file" name="iconimgfile" class="custom-file-input" id="inputGroupFile04">
    <label class="custom-file-label" for="inputGroupFile04">Choose File</label>
  </div>
</div>
                    </div>
                    
                                        <div class="form-group"> <label>Preview Image File (.PNG,.JPG):</label> 
                                   <div class="input-group">
  <div class="custom-file">
    <input type="file" name="previewimgfile" class="custom-file-input" id="inputGroupFile04">
    <label class="custom-file-label" for="inputGroupFile04">Choose File</label>
  </div>
</div>
                    </div>-->                   

<hr>
<button type="submit" id="btn" class="btn btn-primary w-100">Upload</button>

        <div class="progress mt-3" style="display:none;">
              <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">

              </div>
            </div>
<script type="text/javascript">

/*
  $(document).ready(function(){
    $("#postJson").click(function(){
      $('#response').html("<b>Mohon Tunggu , pengambilan data ..</b>");
        $.post("https://php-1-codeanywhere565467.codeanyapp.com/scraper-bukalapak.php",
        {
          name: "Donald Duck",
          city: "Duckburg"
        },
        function(data,status){
          $( "#response" ).remove();
          alert("Sukses Menambahkan Data Bukalapak");
          document.getElementById("duelToegevoegd").value = "Johnny Bravo";
        });
    });
});*/

</script>
<script type="text/javascript">
  $("#upload").on("submit",(function(e) {
      
      tinyMCE.triggerSave();
      
    e.preventDefault();

            e.stopImmediatePropagation();
            
            
            $('.progress').show();
    
    $.ajax({
        
         xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.progress-bar').css('width',percentComplete+"%");
                            $('.progress-bar').html(percentComplete+"%");
                            if (percentComplete === 100) {

                        }
                      }
                    }, false);
                    return xhr;
                  },
        
          url: "<?php echo $setting['website_url'];?>/admin/ajax-upload.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      beforeSend: function() 
        {
            $("#res").html('Uploading..Please wait!');
        },  
        success: function(response)
        {
            $("#res").html(response);
        },  
error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                }
     });
  }));
</script>
<div id="res"></div>
</form>


        <script>
            $('.custom-file-input').on('change', function() { 
                let fileName = $(this).val().split('\\').pop(); 
                $(this).next('.custom-file-label').addClass("selected").html(fileName); 
            });
        </script>                        

     			<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		themes: "modern",
		branding: false,
		  plugins: [
    'advlist autolink lists link image charmap preview',
    'visualblocks code',
    'insertdatetime media contextmenu paste code'
  ],
  toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code'
	});
	</script>   
 <?php 
require_once('includes/footer.php');
?>