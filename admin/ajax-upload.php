<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once('../system/config-admin.php');
if(!preg_match('/^([1-9][0-9]*|0)(\.[0-9]{2})?$/', $_POST['price']))
{
  echo '<span class="text-danger">Ups Gagal , Nilai harga produk belum di isi !</span>';
  die();
}
if(empty($_POST['name']))
{
  echo '<span class="text-danger">Nama Produk Harus di isi!</span>';
  die();
}
if(empty($_POST['sdesc']))
{
  echo '<span class="text-danger">Deskripsi Singkat harus di isi.</span>';
  die();
}
if(empty($_POST['description']))
{
  echo '<span class="text-danger">Deskripsi harus di isi.</span>';
  die();
}
if(empty($_POST['cat_id']))
{
  echo '<span class="text-danger">Kategori harus di isi.</span>';
  die();
}
  if(empty($_POST['gambar_produk'])){
    echo '<span class="text-danger">Gambar Belum Tersedia.</span>';
  }
  
  //print_r($_POST);

/*
if(isset($_FILES['previewimgfile'])){
  $image_name = $_FILES['previewimgfile']['name'];
  $image_name = preg_replace("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.\']/", "", $image_name);
  $tmp_name   = $_FILES['previewimgfile']['tmp_name'];
  $image_type = $_FILES['previewimgfile']['type'];
  $image_size = $_FILES['previewimgfile']['size'];
  $allowed_image = array('image/png', 'image/PNG', 'image/jpeg', 'image/JPEG', 'image/GIF', 'image/gif');
  if(!in_array($image_type, $allowed_image)){
    echo '<span class="text-danger">Please select a jpg/png/gif for preview img!</span>';
    die();
  }
  else
  {
      if($image_size > 5767168){
          echo'file too big';
          die();
      }
      else{
    $new_image_name = time().$image_name;
    move_uploaded_file($tmp_name, '../system/assets/uploads/products/'.$new_image_name.'');
    }
  }
}
if(isset($_FILES['iconimgfile'])){
  $aimage_name = $_FILES['iconimgfile']['name'];
  $aimage_name = preg_replace("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.\']/", "", $aimage_name);
  $tmp_name   = $_FILES['iconimgfile']['tmp_name'];
  $aimage_type = $_FILES['iconimgfile']['type'];
  $aimage_size = $_FILES['iconimgfile']['size'];
  $aallowed_image = array('image/png', 'image/PNG', 'image/jpeg', 'image/JPEG', 'image/GIF', 'image/gif');
  if(!in_array($aimage_type, $aallowed_image)){
    echo '<span class="text-danger">Please select a jpg/png/gif for icon img!</span>';
    die();
  }
  else
  {
      if($aimage_size > 5767168){
          echo'file too big';
          die();
      }
      else{
    $new_image_name1 = time().$aimage_name;
    move_uploaded_file($tmp_name, '../system/assets/uploads/products/'.$new_image_name1.'');
    }
  }
}
if(isset($_FILES['mainfile'])){
  $name = $_FILES['mainfile']['name'];
  $name = preg_replace("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.\']/", "", $name);
  $tmp_name   = $_FILES['mainfile']['tmp_name'];
  $file_size = $_FILES['mainfile']['size'];
if($file_size < 1){
    echo 'No file selcted try again!';
    die();
}
  $file_type = pathinfo($name);
  $file_type = $file_type['extension'];
  $allowed_file = array('zip','jpeg','txt');
  if(!in_array($file_type, $allowed_file)){
    echo '<span class="text-danger">Please select a .zip file for the main file</span>';
  }
  else
  {
    $new_file_name = time().$name;
    move_uploaded_file(''.$tmp_name.'', '../system/assets/uploads/product-files/'.$new_file_name.'');
}
  }
*/
  
$name2 = $_POST['name'];
$sdesc = $_POST['sdesc'];
$description = $_POST['description'];
$cat_id = $_POST['cat_id'];
$scat_id = (isset($_POST['subcat']) ? $_POST['subcat'] : null);
  //gambar utama tokopedia
$demo = $_POST['demo'];
  $demo_bl = $_POST['url_bl'];
  $gambar_produk = $_POST['gambar_produk'];
$price = $_POST['price'];
  $price_bl = $_POST['harga-bl'];
$date = date("Y-m-d");
$featured = (isset($_POST['featured'])) ? 1 : 0;
$support = (isset($_POST['support'])) ? 1 : 0;
$stock_on = (isset($_POST['stock_on'])) ? 1 : 0;
$stock = $_POST['stock'];
$views_off = (isset($_POST['views_off'])) ? 1 : 0;
$reviews_off = (isset($_POST['reviews_off'])) ? 1 : 0;
$free = (isset($_POST['free'])) ? 1 : 0;

$sql_upload = $DB_con->prepare("INSERT INTO dsptesty_products (name, short_des, description, gambar_produk, cat_id, subc_id, price, price_bl, demo, demo_bl, created, modified, support, featured, stock_on, stock, views_off, reviews_off, free, active) VALUES (:name2, :sdesc, :description, :gambar_produk, :cat_id, :scat_id, :price, :price_bl, :demo, :demo_bl, :created, :modified, :support, :featured, :stock_on, :stock, :views_off, :reviews_off, :free, '1')");

$sql_upload->bindparam(":name2",$name2);
$sql_upload->bindparam(":sdesc",$sdesc);
$sql_upload->bindparam(":description",$description);
  $sql_upload->bindparam(":gambar_produk",$gambar_produk);
$sql_upload->bindparam(":cat_id",$cat_id);
$sql_upload->bindparam(":scat_id",$scat_id);
//$sql_upload->bindparam(":icon_img",$new_image_name1);
//$sql_upload->bindparam(":preview_img",$new_image_name);
//$sql_upload->bindparam(":file",$new_file_name);
$sql_upload->bindparam(":price",$price);
  $sql_upload->bindparam(":price_bl",$price_bl);
$sql_upload->bindparam(":demo",$demo);
  $sql_upload->bindparam(":demo_bl",$demo_bl);
 
$sql_upload->bindparam(":created",$date);
$sql_upload->bindparam(":modified",$date);
$sql_upload->bindparam(":support",$support);
$sql_upload->bindparam(":featured",$featured);
$sql_upload->bindparam(":stock_on",$stock_on);
$sql_upload->bindparam(":stock",$stock);
$sql_upload->bindparam(":views_off",$views_off);
$sql_upload->bindparam(":reviews_off",$reviews_off);
$sql_upload->bindparam(":free",$free);

if($sql_upload->execute()){
echo '<span class="text-success">Product Successfully Added!</span>';
}
else{
echo "Error: " . $sql_upload->error;
}

}
else {
  header('location: ../index.php');
}
?>