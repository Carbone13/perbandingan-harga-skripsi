<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
require_once('../system/config-admin.php');

if(!empty($_FILES['previewimgfile']['name'])){
if(isset($_FILES['previewimgfile'])){
  $image_name = $_FILES['previewimgfile']['name'];
  $image_name = preg_replace("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.\']/", "", $image_name);
  $tmp_name   = $_FILES['previewimgfile']['tmp_name'];
  $image_type = $_FILES['previewimgfile']['type'];
  $image_size = $_FILES['previewimgfile']['size'];
  $allowed_image = array('image/png', 'image/PNG', 'image/jpeg', 'image/JPEG', 'image/gif', 'image/GIF');
  if(!in_array($image_type, $allowed_image)){
    echo '<span class="text-danger">Please select a jpg/png/gif for preview img!</span>';
    die();
  }
  else
  {
      if($image_size > 5767168){
          echo'file too big';
          die();
      }
      else{
    $new_image_name = time().$image_name;
    move_uploaded_file($tmp_name, '../system/assets/uploads/products/'.$new_image_name.'');
    }
  }
}
}
if(!empty($_FILES['iconimgfile']['name'])){
if(isset($_FILES['iconimgfile'])){
  $aimage_name = $_FILES['iconimgfile']['name'];
  $aimage_name = preg_replace("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.\']/", "", $aimage_name);
  $tmp_name   = $_FILES['iconimgfile']['tmp_name'];
  $aimage_type = $_FILES['iconimgfile']['type'];
  $aimage_size = $_FILES['iconimgfile']['size'];
  $aallowed_image = array('image/png', 'image/PNG', 'image/jpeg', 'image/JPEG', 'image/gif', 'image/GIF');
  if(!in_array($aimage_type, $aallowed_image)){
    echo '<span class="text-danger">Please select a jpg/png/gif for icon img!</span>';
    die();
  }
  else
  {
      if($aimage_size > 5767168){
          echo'file too big';
          die();
      }
      else{
    $new_image_name1 = time().$aimage_name;
    move_uploaded_file($tmp_name, '../system/assets/uploads/products/'.$new_image_name1.'');
    }
  }
}
}
if(!empty($_FILES['mainfile']['name'])){
if(isset($_FILES['mainfile'])){
  $name = $_FILES['mainfile']['name'];
  $name = preg_replace("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ.\']/", "", $name);
  $tmp_name   = $_FILES['mainfile']['tmp_name'];
  $file_size = $_FILES['mainfile']['size'];
if($file_size < 1){
    echo 'No file selcted try again!';
    die();
}
  $file_type = pathinfo($name);
  $file_type = $file_type['extension'];
  $allowed_file = array('zip','jpeg','txt');
  if(!in_array($file_type, $allowed_file)){
    echo '<span class="text-danger">Please select a .zip file for the main file</span>';
  }
  else
  {
    $new_file_name = time().$name;
    move_uploaded_file(''.$tmp_name.'', '../system/assets/uploads/product-files/'.$new_file_name.'');
}
  }
}


$id = $_POST['id'];
$date = date("Y-m-d");

if ($_FILES['previewimgfile']['name'] !== "") { 

$sql_upload = $DB_con->prepare("UPDATE dsptesty_products SET preview_img=:preview_img WHERE id=:id");

$sql_upload->bindparam(":preview_img",$new_image_name);
$sql_upload->bindparam(":id",$id);

if($sql_upload->execute()){
echo '<span class="text-success">Preview Image Files Updated! </span>';
}
else{
echo "Error: " . $sql_upload->error;
}

}
if ($_FILES['iconimgfile']['name'] !== "") { 

$sql_upload = $DB_con->prepare("UPDATE dsptesty_products SET icon_img=:icon_img WHERE id=:id");

$sql_upload->bindparam(":icon_img",$new_image_name1);
$sql_upload->bindparam(":id",$id);

if($sql_upload->execute()){
echo '<span class="text-success">Icon Image Files Updated! </span>';
}
else{
echo "Error: " . $sql_upload->error;
}

}
if ($_FILES['mainfile']['name'] !== "") { 

$sql_upload = $DB_con->prepare("UPDATE dsptesty_products SET file=:file,modified=:modified WHERE id=:id");

$sql_upload->bindparam(":file",$new_file_name);
$sql_upload->bindparam(":modified",$date);
$sql_upload->bindparam(":id",$id);

if($sql_upload->execute()){
echo '<span class="text-success">Main Files Updated! </span>';
}
else{
echo "Error: " . $sql_upload->error;
}

}

/*$sql_upload = $DB_con->prepare("UPDATE dsptesty_products SET icon_img=:icon_img, preview_img=:preview_img, file=:file WHERE id=:id");

$sql_upload->bindparam(":icon_img",$new_image_name1);
$sql_upload->bindparam(":preview_img",$new_image_name);
$sql_upload->bindparam(":file",$new_file_name);
$sql_upload->bindparam(":id",$id);

if($sql_upload->execute()){
echo '<span class="text-success">Product Files Updated!</span>';
}
else{
echo "Error: " . $sql_upload->error;
}*/

}
else {
  header('location: ../index.php');
}
?>