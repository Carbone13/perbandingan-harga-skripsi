<!doctype html>
<html lang="en">

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="robots" content="index,follow" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="<?php echo $setting['website_url'];?>/system/assets/css/styles.css">
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="<?php echo $setting['website_url'];?>/system/dsp.js"></script>
  <link rel="icon" type="image/png" href="<?php echo $setting['website_url'].'/system/assets/uploads/img/'.$setting['site_favicon']; ?>">
  
  <meta name="author" content="JustChewi">
  <meta itemprop="name" content="<?php echo $setting['site_name'];?>">
  <meta itemprop="image" content="<?php echo $setting['website_url'].'/system/assets/uploads/img/'.$setting['site_favicon']; ?>">
  <meta name="twitter:title" content="<?php echo $setting['site_name'];?>">
  <meta name="twitter:image" content="<?php echo $setting['website_url'].'/system/assets/uploads/img/'.$setting['site_favicon']; ?>">
  <meta property="og:title" content="<?php echo $setting['site_name'];?>">
  <meta property="og:url" content="<?php echo $setting['website_url'];?>">
  <meta property="og:site_name" content="<?php echo $setting['site_name'];?>">
  <meta property="og:image" content="<?php echo $setting['website_url'].'/system/assets/uploads/img/'.$setting['site_favicon']; ?>">

  <title>
    <?php echo $pageTitle . " | " . $setting['site_name']; ?>
  </title>

  <script>
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  </script>

</head>
<body class="homepage">
    
<?php if(!empty($setting['global_message'])){ ?>
<div class="alert box-shadow text-center alert-<?php echo $setting['alert_type'];?> alert-dismissible fade show mb-0 rounded-0" role="alert">
  <?php echo $setting['global_message'];?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php } else{?>
<?php } ?>

<header class="box-shadow1 fixed-top">

  <nav class="navbar p-0 navbar-expand-lg navbar-dark bg-dark1">
    <div class="container">
  <a class="navbar-brand nav-m mr-5" href="<?php echo $setting['website_url'];?>"><?php if(empty($setting['site_logo'])){ ?><?php echo $setting['site_name'];?><?php } else{?><img src="<?php echo $setting['website_url']."/system/assets/uploads/img/".$setting['site_logo']; ?>" width="" height="35"><?php } ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav darkernavhover mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Forum</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Blog</a>
      </li>
    </ul>
    <?php if($user->is_loggedin()){ ?>
    <ul class="navbar-nav ml-auto">
  <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-light" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Hi, <?php echo $userDetails['fname'];?>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
    <a class="dropdown-item" href="<?php echo $setting['website_url'];?>/user/">Overview <span class="badge badge-pill badge-primary"><?php echo $setting['currency_sym'] .  $userDetails['balance'];?></span></a>
    <a class="dropdown-item" href="<?php echo $setting['website_url'];?>/user/downloads.php">Downloads</a>
       <a href="<?php echo $setting['website_url'];?>/user/news.php" class="dropdown-item">News</a>
    <a class="dropdown-item" href="<?php echo $setting['website_url'];?>/user/login.php?logout">Sign Out</a>
        </div>
      </li>
      </ul>
    <?php } else{ ?>
<a href="<?php echo $setting['website_url'];?>/user/" class="btn btn-primary btn-trans mr-2 btn-sm">Login</a>
    <a href="<?php echo $setting['website_url'];?>/user/register.php" class="btn btn-primary btn-sm">Sign Up</a>
    <?php } ?>
  </div>
    </div>
</nav>
   
    <nav class="navbar p-0 navbar-expand-lg navbar-light bg-white">
    <div class="container">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav lightnavhover mr-auto">
      <?php
$category = $product->get_categories();
foreach($category as $cat) {
?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="<?php echo $setting['website_url']; ?>/category/<?php echo $cat['id']; ?>/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $cat['name']; ?>
        </a>
        
        <div class="dropdown-menu box-shadow1" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo $setting['website_url']; ?>/category/<?php echo $cat['id']; ?>/">All <?php echo $cat['name']; ?></a>
           <div class="dropdown-divider"></div>
           <?php $subcat = $product->dispsubcategories($cat['id']); foreach($subcat as $scat) { ?>
          <a class="dropdown-item" href="<?php echo $setting['website_url']; ?>/subcat/<?php echo $scat['id']; ?>/"><?php echo $scat['name']; ?></a>
          <?php } ?>
        </div>

      </li>
      <?php } ?>
    </ul>
    <form action="<?php echo $setting['website_url']; ?>/search.php" method="GET" class="form-inline my-2 my-lg-0">
      <div class="input-group mb-0">
  <input name="key" minlength="3" pattern=".{3,}" required="" type="text" class="form-control form-control-sm header-search" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon2">
  <div class="input-group-append">
    <button class="btn header-searchcustombtn btn-sm" type="button"><i class="fa fa-search"></i></button>
  </div>
</div>
    </form>
  </div>
    </div>
</nav>
</header>
    
