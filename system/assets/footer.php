<footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">Copyright &copy; <?php echo date("Y"); ?> - <?php echo $setting['site_name']; ?></p>
        <ul class="list-inline">
            <?php 
            $cpages = $pages->get_all_pages();
            foreach($cpages as $cpag) {
?>
          <li class="list-inline-item"><a href="<?php echo $setting['website_url'];?>/page/<?php echo $cpag['id']; ?>/"><?php echo $cpag['title']; ?></a></li>
          <?php } ?>
        </ul>
      </footer>

</body>
</html>