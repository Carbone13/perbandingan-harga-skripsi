jQuery(document).ready(function(){

// Set path to PHP source to use in ajax


	var cartpath = "";

// Append PHP source path to <body> for further use
	if(!jQuery("#dsp-cart-path").length){
		jQuery("body").append("<span style=\"display:none;\" id=\"dsp-cart-path\">" + cartpath + "</span>");
	}
	
// Append Modal to handle 
	if(!jQuery("#dsp-buy-dialog").length){
	jQuery("body").append("<div id=\"dsp-buy-dialog\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"aria-labelledby=\"buydialog\" aria-hidden=\"true\"><div class=\"modal-dialog\">    <div class=\"modal-content\"><div class=\"modal-header\"> <h5 class=\"modal-title\"></h5>   <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>  </div>  <div class=\"modal-body\" id=\"dsp-buy-dialog-body\"></div>      <div class=\"modal-footer text-center\"><i class=\"fab fa-cc-visa fa-2x\"></i> <i class=\"fab fa-cc-paypal fa-2x\"></i> <i class=\"fab fa-cc-mastercard fa-2x\"></i> <i class=\"fab fa-cc-discover fa-2x\"></i></div></div>");
	
	}
	
// Init functions to create buttons
	jQuery(".dsp-btn").dspBtn();
	jQuery(".dsp-img").dspImg();
	jQuery(".dsp-desc").dspDesc();
	jQuery(".dsp-info").unbind().bind('click',function(){
	jQuery(this).dspLighbox();
	});

//	Functions in modal
	jQuery("#dsp-buy-btn").click(function(){
	jQuery("#dsp-buy-info").hide();
	jQuery("#dsp-buy-now").fadeIn("slow");
	});
	jQuery(".dsp-show-forgot-pwd").click(function(){
	jQuery("#dsp-buy-now").hide();
	jQuery("#dsp-new-account").hide();
	jQuery("#dsp-forgot-pwd").fadeIn("slow");
	});
	jQuery(".dsp-show-new-account").click(function(){
	jQuery("#dsp-buy-now").hide();
	jQuery("#dsp-forgot-pwd").hide();
	jQuery("#dsp-new-account").fadeIn("slow");
	});
	jQuery(".dsp-show-buy-login").click(function(){
	jQuery("#dsp-forgot-pwd").hide();
	jQuery("#dsp-new-account").hide();
	jQuery("#dsp-buy-now").fadeIn("slow");
	});
	jQuery("#dsp-pay-now").click(function(){
	jQuery("#dsp-pay-now").hide();
	jQuery("#dsp-pay-now-form").fadeIn("slow");
	});
		jQuery("#dsp-pay-now-form").unbind().bind('submit',function(){
	jQuery(".alert").remove();
		if(!jQuery("input[name=gateway]:checked").val()){
		jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger'>Please select a payment method</div>");
		return false;
		}else{
	
			var pro_id = jQuery("#product_id_div").html();
			var serialized = jQuery(this).serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: cartpath + "/system/assets/ajax/buy.php",
	  		data:serialized + "&pay_now=true&ajax=true",
	  		success: function(html){
			if(html =="success"){
			top.location.href= cartpath + "/user/process.php?action=process";	
			}else{
			jQuery("#buy-dialog-body").prepend("<div class='alert alert-error error'>"+html+"</div>");
			}
		}
	});
	return false;
	}
	});	
		jQuery("#dsp-use-prepaid").unbind().bind('click',function(){
		var r=confirm("Amount will be deducted from prepaid account balance.\r\nContinue");
			if (r!=true)
			{
			jQuery(".alert").remove();
			jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger text-center'>Purchase Cancelled!</div>");
			jQuery("#dsp-pay-now").fadeIn("slow");
			return false;
			}else{			
			jQuery('#dsp-buy-dialog .modal-body').prepend("<div id='dsp-loading' class='text-center'><img src='"+cartpath + "/system/assets/uploads/img/loadingAnimation.gif' /></div>");
		
		jQuery("#dsp-pay-now-form").hide();
		var pro_id = jQuery("#product_id_div").html();
	jQuery.ajax({
	  		type: "POST",
	  		url: cartpath + "/system/assets/ajax/buy.php",
	  		data:"product_id="+pro_id + "&buy=true&ajax=true",
	  		success: function(html){
			if(html =="success"){
			top.location.href=cartpath + '/user/downloads.php';
			}else{
			jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger text-center'>"+html+"</div>");
			}
		}
	});
	return false;
	}
	return false;
	});
	
	
	jQuery("input[name=gateway]").change(function(){
		jQuery(".gateways td").removeClass("selected text-success");
		jQuery(this).parent().addClass("selected text-success");
	});

	
		jQuery("#dsp-buy-login-form").unbind().bind('submit',function(){
		jQuery('#dsp-buy-dialog .modal-body').prepend("<div id='dsp-loading' class='text-center'><img src='"+cartpath + "/system/assets/uploads/img/loadingAnimation.gif' /></div>");
	jQuery("#dsp-buy-dialog-body .alert").remove();
	var email = jQuery("#inputEmail").val();
	var password = jQuery("#inputPassword").val();
	if(email == "" || password ==""){
	jQuery("#dsp-loading").remove();
	jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger text-center'>Please input Email and Password</div>");
	return false;
	}else{
	
	var pro_id = jQuery("#product_id_div").html();
	var serialized = jQuery(this).serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: cartpath + "/system/assets/ajax/buy.php",
	  		data:serialized + "&ajax=true",
	  		success: function(html){
			if(html =="success"){
			jQuery("#dsp-buy-dialog-body").load(cartpath + "/system/assets/ajax/buy.php?product_id="+pro_id ,function(){
			jQuery("#dsp-buy-info").hide();
			jQuery("#dsp-buy-now").fadeIn("slow");
			jQuery.getScript(cartpath + '/system/dsp.js');
			});			
			}else{
			jQuery("#dsp-loading").remove();
			jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger text-center'>"+html+"</div>");
			}
		}
	});
	return false;
	}
	});	
	
		jQuery("#dsp-new-user").unbind().bind('submit',function(){
	jQuery('#dsp-buy-dialog .modal-body').prepend("<div id='dsp-loading' class='text-center'><img src='"+cartpath + "/system/assets/uploads/img/loadingAnimation.gif' /></div>");
	jQuery("#dsp-buy-dialog-body .alert").remove();
	var email = jQuery("#new-email").val();
	var password = jQuery("#new-pwd").val();
	var password2 = jQuery("#new-pwd2").val();
	if(email == "" || password =="" || password!=password2){
	jQuery("#buy-dialog-body").prepend("<div class='alert alert-danger text-center'>All fields are required</div>");
	jQuery("#dsp-loading").remove();
	return false;
	}else{
	var pro_id = jQuery("#product_id_div").html();
	var serialized = jQuery(this).serialize();
		jQuery.ajax({
	  		type: "POST",
	  		url: cartpath + "/user/new.php",
	  		data:serialized + "&signup=true&ajax=true",
	  		success: function(html){
			if(html =="success"){
			jQuery("#dsp-buy-dialog-body").load(cartpath + "/system/assets/ajax/buy.php?product_id="+pro_id ,function(){
			jQuery("#dsp-buy-info").hide();
			jQuery("#dsp-buy-now").show();
			jQuery.getScript(cartpath + '/system/dsp.js');
			});			
			}else{
			jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger text-center'>"+html+"</div>");
			jQuery("#dsp-loading").remove();
			}
		}
	});
	return false;
	}
	});
		jQuery("#dsp-coupons-form").unbind().bind('submit',function(){
		var pro_id = jQuery("#product_id_div").html();
			jQuery('#dsp-buy-dialog .modal-body').prepend("<div id='dsp-loading' class='text-center'><img src='"+cartpath + "/system/assets/uploads/img/loadingAnimation.gif' /></div>");
	jQuery("#dsp-buy-dialog-body .alert").remove();
		var couponCode = jQuery("#dsp-coupon-code").val();
		var jsonUrl = cartpath + "/system/assets/ajax/coupons.php";
		jQuery.get( jsonUrl, { discountcoupon: couponCode }).done(function( data ) {
		var jsonData = jQuery.parseJSON(data);
		if(typeof jsonData['error'] == 'undefined'){
		jQuery("#dsp-buy-dialog-body").load(cartpath + "/system/assets/ajax/buy.php?product_id="+pro_id ,function(){
			jQuery("#dsp-buy-info").hide();
			jQuery("#dsp-buy-now").show();
			jQuery.getScript(cartpath + '/system/dsp.js');
			});
		}else{
		jQuery("#dsp-buy-dialog-body").prepend("<div class='alert alert-danger text-center'>"+jsonData.error+"</div>");
		jQuery("#dsp-loading").remove();
		return false;
		}
		});
		return false;
	});
	jQuery("#dsp-coupon-remove").unbind().bind('click',function(){
		 jQuery("#dsp-coupon-code").val(null);
		jQuery("#dsp-coupons-form").submit();
		return false;
		});
// end jQuery(document).ready()		
		}); 
// functions
 //Buttons
jQuery.fn.dspBtn = function(){
 var cartpath = jQuery("#dsp-cart-path").html();
	this.each(function(){
		var id = jQuery(this).attr('data-pid');
		var button_text = jQuery(this).attr('data-text');
		var price = jQuery(this).attr('data-price');
		var name = jQuery(this).attr('data-name');
		jQuery(this).removeClass('dsp-btn');
		var jsonUrl = cartpath + "/system/assets/ajax/json-load.php";
		var curr = jQuery(this) ;
		jQuery(this).html('Initiating...');
		jQuery.get( jsonUrl, { product_id: id, type: "btn" }).done(function( data ) {
		var jsonData = jQuery.parseJSON(data);
		if(typeof jsonData['error'] == 'undefined'){
			var markup = "<form class=\"dsp-form form-horizontal\" method=\"post\">";
			markup +="<input name=\"product_id\" type=\"hidden\" value=\"" +id+"\">";
			if(name =='show'){
			markup +="<span class='dsp-name'>"+jsonData.name+"</span> ";
			}
			if(price =='show'){
			markup +="<span class='dsp-price'>Price: "+jsonData.currency+"&nbsp;"+jsonData.price+"</span>";
			}
			if(button_text !=null){
			markup +=" <button class='btn btn-primary btn-lg btn-block font-bold mt-4' type=\"submit\" >"+button_text+"</button>";
			}else{
			markup +=" <button class='btn btn-primary btn-lg btn-block font-bold mt-4' type=\"submit\" >Buy Now</button>";
			}
			markup +="</form>";
			curr.html(markup);
		}else{
		curr.html(jsonData.error);
		return false;
		}
			});
	});
	jQuery.getScript(cartpath + '/system/dsp.js');
	jQuery(".dsp-form").unbind().bind('submit',function(){
	jQuery(this).dspBuy();
	return false;
	});
return false;
};
 //Images
jQuery.fn.dspImg = function(){
// extract previously saved cartpath
 var cartpath = jQuery("#dsp-cart-path").html();
	this.each(function(){
		var id = jQuery(this).attr('data-pid');
		var size = jQuery(this).attr('data-size');
		var responsive = jQuery(this).attr('data-responsive');
		var shape = jQuery(this).attr('data-shape');
		var w = jQuery(this).attr('data-w');
		var h = jQuery(this).attr('data-h');
		var jsonUrl = cartpath + "/system/assets/ajax/json-load.php";
		jQuery(this).addClass('cart-image');
		jQuery(this).removeClass('dsp-img');
		var curr = jQuery(this) ;
		jQuery(this).html('Loading Image..');
		jQuery.get( jsonUrl, { product_id: id, type: "img" }).done(function( data ) {
		var jsonData = jQuery.parseJSON(data);
		if(typeof jsonData['error'] == 'undefined'){
			var markup = "<img ";
			if(size =='small' || size =='medium' || size =='large'){
			jsonData.image =  size+'-'+jsonData.image;
			}
			markup +="src ='"+cartpath+"/system/assets/uploads/products/"+jsonData.image+"' ";
			markup +="style=\"";
			if(w !=null){
			markup +=" width:"+w+"px;";
			}
			if(h !=null){
			markup +=" height:"+h+"px;";
			}
			markup +="\"";
			markup += "/>";
			curr.html(markup);
			if(responsive !=null){
			curr.children('img').addClass('img-responsive');
			}
			if(shape == 'rounded' || shape == 'circle' || shape == 'thumbnail'){
			curr.children('img').addClass('img-'+shape);
			}
		}else{
		curr.html(jsonData.error);
		return false;
		}
		});
	});
return false;
};
	//Description
jQuery.fn.dspDesc = function(){
// extract previously saved cartpath
 var cartpath = jQuery("#dsp-cart-path").html();
	this.each(function(){
		var id = jQuery(this).attr('data-pid');
		jQuery(this).addClass('cart-description');
		jQuery(this).removeClass('dsp-desc');
		var jsonUrl = cartpath + "/system/assets/ajax/json-load.php";
		var curr = jQuery(this) ;
		jQuery.get( jsonUrl, { product_id: id, type: "desc" }).done(function( data ) {
		var jsonData = jQuery.parseJSON(data);
		if(typeof jsonData['error'] == 'undefined'){
		curr.html(jsonData.desc);
		}else{
		curr.html(jsonData.error);
		return false;
		}
		});
	});
return false;
};
jQuery.fn.dspLighbox = function(){
// extract previously saved cartpath
 var cartpath = jQuery("#dsp-cart-path").html();
  jQuery('#dsp-buy-dialog .modal-title').html("Product details");
 jQuery('#dsp-buy-dialog .modal-body').html("<div class='text-center'><img src='"+cartpath + "/system/assets/uploads/img/loadingAnimation.gif' /></div>");
	jQuery('#dsp-buy-dialog').modal();

		var id = jQuery(this).attr('data-pid');
		var jsonUrl = cartpath + "/system/assets/ajax/json-load.php";
		var curr = jQuery(this) ;
		jQuery.get( jsonUrl, { product_id: id, type: "all" }).done(function( data ) {
		var jsonData = jQuery.parseJSON(data);
		if(typeof jsonData['error'] == 'undefined'){
		var markup = "<div class='row'><div class='col-md-4 text-center'>";
		markup += "<img class='img-responsive img-thumbnail' src='"+cartpath + "/system/assets/uploads/products/"+jsonData.image+"' /></div>";
		markup +="<div class='col-md-8'><h3 class='no-top'>"+jsonData.name+"</h3>";
		markup +="<h4>Price"+jsonData.currency+"&nbsp;"+jsonData.price+"</h4>";
		markup +="<p>"+jsonData.desc+"</p>";
		markup += "<form class=\"dsp-form form-horizontal\" method=\"post\">";
		markup +="<input name=\"product_id\" type=\"hidden\" value=\"" +id+"\">";
		markup +=" <button class='btn btn-primary btn-lg btn-block font-bold mt-4' type=\"submit\" >Buy Now</button>";
		markup +="</form>";
		markup +="</div></div>";
		jQuery('#dsp-buy-dialog .modal-body').html(markup);
		}else{
		 jQuery('#dsp-buy-dialog .modal-body').html("<div class='alert alert-danger'>"+jsonData.error+"</div>");
		return false;
		}
		});

return false;
};
jQuery.fn.dspBuy = function(){
// extract previously saved cartpath
 var cartpath = jQuery("#dsp-cart-path").html();
 jQuery('#dsp-buy-dialog .modal-title').html("Item Purchase");
jQuery('#dsp-buy-dialog .modal-body').html("<div class='text-center'><img src='"+cartpath + "/system/assets/uploads/img/loadingAnimation.gif' /></div>");
	jQuery('#dsp-buy-dialog').modal();
	var form = jQuery(this);
	var serialized = jQuery(form).serialize();
	jQuery.ajax({
					type:"POST",
					url: cartpath + "/system/assets/ajax/buy.php",
					data:serialized ,
					success: function(html){
					jQuery('#dsp-buy-dialog .modal-body').html(html);
					jQuery.getScript(cartpath + '/system/dsp.js');
					}
				});
return false;
};