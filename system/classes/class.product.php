<?php 

class Product{
	var $error = false;
	var $msg = false;
	
		private $db;
	
	function __construct($DB_con)
	{
		$this->db = $DB_con;
	}
	
	public function all(){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 ORDER BY `id` DESC");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
		return $products;
		}
		
		public function getProducts($start,$total){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 OR active = 2 ORDER BY `id` DESC LIMIT $start , $total");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getCProducts($id = null, $cat_id = null){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 AND cat_id = '$cat_id' ORDER BY `id` DESC");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getCSProducts($id = null, $sub_id = null){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 AND subc_id = '$sub_id' ORDER BY `id` DESC");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getFeaturedProducts(){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 AND featured = 1 ORDER BY `id` LIMIT 3");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getPopularProducts(){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 AND sales > 0 ORDER BY sales ASC,views DESC LIMIT 6");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getNewProducts(){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 ORDER BY `id` DESC LIMIT 9");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getSimilarProducts($cat_id){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE cat_id = $cat_id LIMIT 3");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

		public function getFreeProducts(){
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 AND free = 1 ORDER BY `id` DESC LIMIT 6");
    $result->execute();
	$products = array();
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$products[]=$row;
		}
	return $products;
}

	public function countAll(){
	    
	    $result = $this->db->prepare("SELECT count(*) FROM  " . PFX . "products WHERE active = 1 ORDER BY `id` DESC");
        $result->execute();
		$products = $result->fetchColumn();
	    return $products;
}	

public function top(){
	
	$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE active = 1 ORDER BY `sales` DESC LIMIT 0 , 10");
		$result->execute();
	    $products = array();
	    while($row=$result->fetch(PDO::FETCH_ASSOC)){
		$products[]=$row;
	    }	
	    return $products;
	
}

	public function is_product($id){
		
		$result = $this->db->prepare("SELECT active FROM  " . PFX . "products WHERE id = '$id' AND  active = 1");
		$result->execute();
		
		if ($result){
    	return true;
		}
		$this->error = "No such product exists";
		return false;
		
	}
	
	public function details($id){
		if($this->is_product($id)){
			
			$result = $this->db->prepare("SELECT * FROM  " . PFX . "products WHERE id = :id");
          $result->bindParam(':id', $id);
		$result->execute();
			
			while($result=$result->fetch(PDO::FETCH_ASSOC)){
			return $result;
			}
			}
		return false;
	}
		public function catdetails($id){
			
			$result = $this->db->prepare("SELECT * FROM  " . PFX . "categories WHERE id = :id");
          $result->bindParam(':id', $id);
		$result->execute();
			
			while($result=$result->fetch(PDO::FETCH_ASSOC)){
			return $result;
			}
	}
			public function scatdetails($id){
			
			$result = $this->db->prepare("SELECT * FROM  " . PFX . "subcat WHERE id = :id");
          $result->bindParam(':id', $id);
		$result->execute();
			
			while($result=$result->fetch(PDO::FETCH_ASSOC)){
			return $result;
			}
	}
	
	public function add_sale($id){
		if($this->is_product($id)){
		$sales = $this->details($id);
		$sales = $sales['sales'] + 1;
		$update = $this->db->prepare("UPDATE " . PFX . "products  SET `sales` = '$sales' WHERE id ='$id'");
    		$update->execute();
		if($update){
		return true;
		}
		return false;
		}
	}
	
	public function remove($id){
	$detail = $this->details($id);
		if($this->is_product($id)){
		    
		$update = $this->db->prepare("UPDATE " . PFX . "products  SET `active` = '0' WHERE id ='$id'");
		$update->execute();
	if($update){
		$this->msg = "Product removed successfully";
		return true;
	}
	$this->error = "Error removing product";
	return false;
	}
	$this->error = "Error removing product";
	return false;
	}
//Categories start here	
	public function countAllCat(){
	    
	    $result = $this->db->prepare("SELECT count(*) FROM  " . PFX . "categories ORDER BY `id` DESC");
        $result->execute();
		$category = $result->fetchColumn();
	    return $category;
}

	public function countAllSCat(){
	    
	    $result = $this->db->prepare("SELECT count(*) FROM  " . PFX . "subcat ORDER BY `id` DESC");
        $result->execute();
		$category = $result->fetchColumn();
	    return $category;
}	

public function countAllCatDeleted(){
    
    	$result = $this->db->prepare("SELECT count(*) FROM  " . PFX . "categories WHERE active = 0");
		$result->execute();
		$categories = $result->fetchColumn();
	    return $categories;
	
}

public function countAllCatSDeleted(){
    
    	$result = $this->db->prepare("SELECT count(*) FROM  " . PFX . "subcat WHERE active = 0");
		$result->execute();
		$categories = $result->fetchColumn();
	    return $categories;
	
}

public function get_categories($id = null){
   
   $query = $this->db->prepare("SELECT * FROM `dsptesty_categories` WHERE active = 1");
   $query->execute();
   $categories = array();
   while($row=$query->fetch(PDO::FETCH_ASSOC)){
    $categories[] = $row;

   }
   
   return $categories;
}

public function get_scategories($id = null){
   
   $query = $this->db->prepare("SELECT * FROM `dsptesty_subcat` WHERE active = 1");
   $query->execute();
   $categories = array();
   while($row=$query->fetch(PDO::FETCH_ASSOC)){
    $categories[] = $row;

   }
   
   return $categories;
}

public function dispsubcategories($parent_id) {

$query = $this->db->prepare("SELECT * FROM dsptesty_subcat WHERE active = 1 AND cat_id = :parent_id ORDER BY name ASC");
$query->bindParam(':parent_id', $parent_id);
   $query->execute();
$scategories = array();
		 while($row=$query->fetch(PDO::FETCH_ASSOC)){
		     $scategories[] = $row;
		}
		  return $scategories;
	}

public function getReviews($id){
   
   $query = $this->db->prepare("SELECT * FROM `dsptesty_reviews` WHERE product_id = $id AND status = 1");
   $query->execute();
   $reviews = array();
   while($row=$query->fetch(PDO::FETCH_ASSOC)){
    $reviews[] = $row;
   }
   
   return $reviews;
}

public function is_alreadyadd($userID,$productID){

		$result = $this->db->prepare("SELECT * FROM  " . PFX . "reviews WHERE user_id = '$userID' AND product_id = '$productID' AND status = '1'");
		$result->execute();
		if ($result->fetchColumn() >= 1){
    	return true;
		}
		$this->error = "No such review exists";
		return false;
	
}

	public function countReview($pid){
	    
	    $result = $this->db->prepare("SELECT count(id) FROM  " . PFX . "reviews WHERE product_id = ? AND status = 1");
        $result->execute(array($pid));
		$reviews = $result->fetchColumn();
	    return $reviews;
}

	public function addreview($pid,$uid,$message1,$rating){
    
		$message1 = trim($message1);
	    if(empty($message1)){
		$this->error = 'Please input all details';
		return false;
		}
		$add = $this->db->prepare("INSERT INTO " . PFX . "reviews (`product_id`, `user_id`, `review`, `rating`, `status`) VALUES ('$pid', '$uid', '$message1', '$rating', '1')");
		$add->execute();
	    	if($add){
		$this->msg = "Review added successfully";
		return true;
		}	
		$this->error = 'Review hmmm';
		return false;	
		
}
	
	public function addcat($name){
    
		$name = trim($name);
	    if(empty($name)){
		$this->error = 'Please input all details';
		return false;
		}
		$add = $this->db->prepare("INSERT INTO " . PFX . "categories (`id`, `name`, `active`) VALUES (NULL, :name, '1')");
      	$add->bindParam(':name', $name);
		$add->execute();
	    	if($add){
		$this->msg = "Category added successfully";
		return true;
		}	
		$this->error = 'Category saved';
		return false;	
		
}

	public function addscat($name,$cid){
    
		$name = trim($name);
	    if(empty($name)){
		$this->error = 'Please input all details';
		return false;
		}
		$add = $this->db->prepare("INSERT INTO " . PFX . "subcat (`name`, `cat_id`, `active`) VALUES (:name, :cid, '1')");
      	$add->bindParam(':name', $name);
      	$add->bindParam(':cid', $cid);
		$add->execute();
	    	if($add){
		$this->msg = "SubCategory added successfully";
		return true;
		}	
		$this->error = 'SubCategory saved';
		return false;	
		
}

public function updatecat($id,$name){
    
		$name = trim($name);
		if($this->is_cat($id)){
		if(empty($name)){
		$this->error = 'Please input all details';
		return false;
		}
		$update = $this->db->prepare("UPDATE " . PFX . "categories  SET `name` = :name WHERE id = :id");
        $update->bindParam(':name', $name);
        $update->bindParam(':id', $id);
		$update->execute();
		if($update){
		$this->msg = "Category updated successfully";
		return true;
	}
	    	$this->error = "Error saving Category";
	    	return false;
	    	}
    		$this->error = "Error saving Category";
	    	return false;

}

	public function is_cat($id){
		
		$result = $this->db->prepare("SELECT active FROM  " . PFX . "categories WHERE id = '$id' AND  active = 1");
		$result->execute();
		
		if ($result){
    	return true;
		}
		$this->error = "No such category exists";
		return false;
		
	}
	
	public function updatescat($id,$name,$cid){
    
		$name = trim($name);
		if($this->is_scat($id)){
		if(empty($name)){
		$this->error = 'Please input all details';
		return false;
		}
		$update = $this->db->prepare("UPDATE " . PFX . "subcat  SET `name` = :name, `cat_id` = :cid WHERE id = :id");
        $update->bindParam(':name', $name);
        $update->bindParam(':cid', $cid);
        $update->bindParam(':id', $id);
		$update->execute();
		if($update){
		$this->msg = "SubCategory updated successfully";
		return true;
	}
	    	$this->error = "Error saving SubCategory";
	    	return false;
	    	}
    		$this->error = "Error saving SubCategory";
	    	return false;

}

	public function is_scat($id){
		
		$result = $this->db->prepare("SELECT active FROM  " . PFX . "subcat WHERE id = '$id' AND  active = 1");
		$result->execute();
		
		if ($result){
    	return true;
		}
		$this->error = "No such subcategory exists";
		return false;
		
	}
	
public function removecat($id){
    
		if($this->is_cat($id)){
		$update = $this->db->prepare("UPDATE " . PFX . "categories  SET `active` = '0' WHERE id ='$id'");
		$update->execute();
								
	    if($update){
		$this->msg = "Category Removed Successfully";
		return true;
	    }
	    $this->error = "Error removing Category";
	    return false;
	    }
	    $this->error = "Error removing Category";
	    return false;
	    
}	
public function removescat($id){
    
		if($this->is_cat($id)){
		$update = $this->db->prepare("UPDATE " . PFX . "subcat  SET `active` = '0' WHERE id ='$id'");
		$update->execute();
								
	    if($update){
		$this->msg = "SubCategory Removed Successfully";
		return true;
	    }
	    $this->error = "Error removing SubCategory";
	    return false;
	    }
	    $this->error = "Error removing SubCategory";
	    return false;
	    
}

public function getDeletedCategories($start,$total){
	
		$result = $this->db->prepare("SELECT * FROM  " . PFX . "categories WHERE active = 0 ORDER BY `id` DESC LIMIT $start , $total");
		$result->execute();
		$categories = array();
	    while($row=$result->fetch(PDO::FETCH_ASSOC)){
        $categories[]=$row;
	    }
	    return $categories;
	    
}

public function getDeletedSubCategories(){
	
		$result = $this->db->prepare("SELECT * FROM  " . PFX . "subcat WHERE active = 0 ORDER BY `id`");
		$result->execute();
		$categories = array();
	    while($row=$result->fetch(PDO::FETCH_ASSOC)){
        $categories[]=$row;
	    }
	    return $categories;
	    
}

public function restorecat($id){
		
		$update = $this->db->prepare("UPDATE " . PFX . "categories  SET `active` = '1' WHERE id ='$id'");
		$update->execute();
								
	    if($update){
		$this->msg = "Category restored successfully";
		return true;
	    }
	    $this->error = "Error restoring category";
	    return false;
	    
}

public function restorescat($id){
		
		$update = $this->db->prepare("UPDATE " . PFX . "subcat  SET `active` = '1' WHERE id ='$id'");
		$update->execute();
								
	    if($update){
		$this->msg = "SubCategory restored successfully";
		return true;
	    }
	    $this->error = "Error restoring subcategory";
	    return false;
	    
}
	
}
?>